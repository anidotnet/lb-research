package org.dizitart.gradle

import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.tasks.Copy
import org.gradle.api.tasks.Delete
import org.gradle.api.tasks.SourceSetContainer
import org.gradle.plugins.ide.idea.IdeaPlugin
import java.io.File

/**
 *
 * @author Anindya Chatterjee
 */
open class LetterBoxBuild : Plugin<Project> {
    override fun apply(project: Project) {
        addVersionInfo(project)

        val projectDir = project.projectDir.absoluteFile.path
        when {
            projectDir.contains("themes") -> copyPlugins(project)
            projectDir.contains("components") -> copyPlugins(project)
        }

        copyLibs(project)
        distCleanUp(project)
    }

    private fun addVersionInfo(project: Project) {
        project.tasks.create("addVersion", AddVersionTask::class.java)
        project.tasks.findByPath("compileKotlin")?.dependsOn("addVersion")

        val file = project.file("${project.buildDir}/version-generated/source/main")

        val sourceSets = project.properties["sourceSets"] as SourceSetContainer
        sourceSets.getByName("main").java.srcDir(file)

        project.plugins.withType(IdeaPlugin::class.java) {
            it.model.module {
                it.generatedSourceDirs.add(file)
            }
        }
    }

    private fun copyPlugins(project: Project) {
        val pluginDir = File("${project.rootDir}/dist/plugins")
        if (!pluginDir.exists()) {
            pluginDir.mkdirs()
        }

        val jarDir = File("${project.buildDir}/libs")

        val copyPlugin = project.tasks.create("copyPlugin", Copy::class.java)
        copyPlugin.from(jarDir)
        copyPlugin.into(pluginDir)

        project.tasks.findByPath("build")?.dependsOn(copyPlugin)

        addDependencies(project, pluginDir)
    }

    private fun copyLibs(project: Project) {
        val libDir = File("${project.rootDir}/dist/libs")
        if (!libDir.exists()) {
            libDir.mkdirs()
        }

        val runtime = project.configurations.getByName("runtime")
        val copyLibs = project.tasks.create("copyLibs", Copy::class.java)
        copyLibs.from(runtime)
        copyLibs.into(libDir)

        project.tasks.findByPath("build")?.dependsOn(copyLibs)

//        addDependencies(project, libDir)
    }

    private fun distCleanUp(project: Project) {
        val distDir = File("${project.rootDir}/dist")
        val cleanDist = project.tasks.create("cleanDist", Delete::class.java) {
            project.delete(distDir)
        }

//        project.tasks.findByPath("clean")?.dependsOn(cleanDist)
    }

    private fun addDependencies(project: Project, dir: File) {
        project.afterEvaluate {
            project.dependencies.add("compile", project.fileTree(dir))
        }
    }
}