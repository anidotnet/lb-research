package org.dizitart.letterbox.api.styles

import javafx.scene.paint.Color

/**
 *
 * @author Anindya Chatterjee
 */
interface LetterBoxPalette : ColorPalette {
    val transparent: Color
    val controlBackground: Color
    val controlBorder: Color
    val imageButtonBorder: Color
    val imageButtonBorderHover: Color
    val imageButtonBorderFocused: Color
    val imageButtonBackground: Color
    val imageButtonBackgroundHover: Color
    val imageButtonBackgroundFocused: Color
    val buttonBorder: Color
    val buttonBorderHover: Color
    val buttonBorderFocused: Color
    val buttonBackground: Color
    val buttonBackgroundHover: Color
    val buttonBackgroundFocused: Color
    val buttonText: Color
    val buttonTextHover: Color
    val buttonTextFocused: Color
}