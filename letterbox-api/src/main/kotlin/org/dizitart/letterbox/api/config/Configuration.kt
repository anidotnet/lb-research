package org.dizitart.letterbox.api.config

import kotlin.properties.ReadWriteProperty

/**
 * @author Anindya Chatterjee.
 */
interface Configuration {
    fun <T: Any> configElement(id: String, initialValue: T? = null): ReadWriteProperty<Any, T?>
            = ConfigDelegate(id, initialValue)
}

interface EditableConfiguration : Configuration {
    val groupHierarchy: Array<String>
    fun getView(): ConfigView?
}