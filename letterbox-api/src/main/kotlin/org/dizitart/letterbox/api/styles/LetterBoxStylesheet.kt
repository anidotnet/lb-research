package org.dizitart.letterbox.api.styles

import javafx.scene.control.ContentDisplay
import javafx.scene.effect.DropShadow
import javafx.scene.layout.BorderStrokeStyle
import javafx.scene.paint.Color
import javafx.scene.text.FontWeight
import tornadofx.*

/**
 *
 * @author Anindya Chatterjee
 */
abstract class LetterBoxStylesheet(override val colorPalette: LetterBoxPalette) : PaletteAwareStylesheet() {
    companion object {
        val controlButton by cssclass()
        val lbMenuButton by cssclass()
        val lbImageButton by cssclass()
        val lbImageButtonNormal by cssclass()
        val lbButton by cssclass()
        val lbButtonNormal by cssclass()

        val acceleratorText by cssclass()

        val window by cssclass()
        val windowMaximized by cssclass()
        val titleBar by cssclass()
    }

    init {
        //region Window

        window {
            backgroundRadius += box(5.px)
            backgroundInsets += box(7.px)
            padding = box(7.px)
            effect = DropShadow()
        }

        windowMaximized {
            backgroundRadius += box(5.px)
            backgroundInsets += box(0.px)
            padding = box(0.px)
            effect = DropShadow()
        }

        //endregion

        //region Button

        lbButton {
            // change color
            borderWidth = multi(box(1.px), box(1.px))
            borderRadius += box(2.px)
            backgroundRadius += box(2.px)
        }

        lbButtonNormal {
            borderColor += box(colorPalette.buttonBorder)
            backgroundColor += colorPalette.buttonBackground
            textFill = colorPalette.buttonText

            and(hover) {
                borderColor += box(colorPalette.buttonBorderHover)
                backgroundColor += colorPalette.buttonBackgroundHover
                textFill = colorPalette.buttonTextHover
                fontWeight = FontWeight.MEDIUM
            }

            and(focused) {
                borderColor += box(colorPalette.buttonBorderFocused)
                backgroundColor += colorPalette.buttonBackgroundFocused
                textFill = colorPalette.buttonTextFocused
                fontWeight = FontWeight.MEDIUM
            }
        }

        lbImageButton {
            borderWidth = multi(box(0.px), box(0.px))
            borderRadius += box(0.px)
            borderStyle += BorderStrokeStyle.NONE
            backgroundRadius += box(0.px)
            contentDisplay = ContentDisplay.GRAPHIC_ONLY
        }

        lbImageButtonNormal {
            borderColor += box(colorPalette.imageButtonBorder)
            backgroundColor += colorPalette.imageButtonBackground

            and(hover) {
                borderColor += box(colorPalette.imageButtonBorderHover)
                backgroundColor += colorPalette.imageButtonBackgroundHover
            }

            and(focused) {
                borderColor += box(colorPalette.imageButtonBorderFocused)
                backgroundColor += colorPalette.imageButtonBackgroundFocused
            }
        }

        controlButton {
            maxWidth = 30.px
            minWidth = 30.px
        }

        s(lbMenuButton child arrowButton) {
            padding = box(0.px)
        }

        s(lbMenuButton child arrowButton child arrow) {
            padding = box(0.px)
        }

        //endregion

        //region ContextMenu

        contextMenu {
            backgroundColor += Color.TEAL
            textFill = Color.WHITE

            menuItem child label {
                textFill = Color.WHITE
            }

            s(menuItem and focused, menuItem and focused child label) {
                backgroundColor += Color.LIGHTPINK
                textFill = Color.BLUEVIOLET
            }

            menuItem contains graphicContainer {
                padding = box(0.em, 1.5.em, 0.em, 0.em)
            }

            menuItem child acceleratorText {
                padding = box(0.px, 0.px, 0.px, 30.px)
                raw("-fx-graphic: none")
            }
        }

        //endregion
    }
}