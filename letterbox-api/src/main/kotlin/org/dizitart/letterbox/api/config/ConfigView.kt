package org.dizitart.letterbox.api.config

import javafx.beans.property.SimpleObjectProperty
import org.dizitart.letterbox.common.di.LetterBoxContext
import tornadofx.Fragment

/**
 * @author Anindya Chatterjee.
 */
abstract class ConfigView : Fragment() {

    @Suppress("UNCHECKED_CAST")
    fun <T: Any> configElement(id: String): SimpleObjectProperty<T?> {
        val configRepository = LetterBoxContext.getBean(ConfigRepository::class)!!
        val configElement = configRepository[id] as ConfigElement<T>
        return configElement.valueProperty
    }
}