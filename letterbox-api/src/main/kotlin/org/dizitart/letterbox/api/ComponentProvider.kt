package org.dizitart.letterbox.api

import javafx.scene.control.MenuItem
import tornadofx.*

/**
 *
 * @author Anindya Chatterjee
 */
interface ComponentProvider {
    val name: String
    val enabled: Boolean
    val ordinal: Int
    val tabGraphics: CssRule
    val pageView: View
    val searchView: View
    val actionView: View
    val menu: Set<MenuItem>

    fun init()
}