package org.dizitart.letterbox.api.plugin

/**
 * @author Anindya Chatterjee.
 */
@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS)
annotation class PluginManifest(val uniqueId: String, val version: String, val dependsOn: Array<Dependency> = [])

@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS)
annotation class Dependency(val pluginId: String, val versionSupport: String = "*")