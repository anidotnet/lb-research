package org.dizitart.letterbox.api.styles

import tornadofx.cssclass

/**
 *
 * @author Anindya Chatterjee
 */
abstract class LetterBoxGraphicsCatalog : GraphicsCatalog {
    companion object {
        val closeButton by cssclass()
        val maximizeButton by cssclass()
        val minimizeButton by cssclass()
        val menuButton by cssclass()
        val notificationButton by cssclass()
    }

    abstract val closeButtonIcon: ButtonGraphics
    abstract val maximizeButtonIcon: ButtonGraphics
    abstract val minimizeButtonIcon: ButtonGraphics
    abstract val menuButtonIcon: ButtonGraphics
    abstract val notificationButtonIcon: ButtonGraphics

    override fun loadCatalog() {
        this.registerElement(closeButton, closeButtonIcon)
        this.registerElement(maximizeButton, maximizeButtonIcon)
        this.registerElement(minimizeButton, minimizeButtonIcon)
        this.registerElement(menuButton, menuButtonIcon)
        this.registerElement(notificationButton, notificationButtonIcon)
    }
}