package org.dizitart.letterbox.api.config

import javafx.beans.property.SimpleObjectProperty
import org.dizitart.letterbox.common.di.LetterBoxContext
import tornadofx.*

/**
 * @author Anindya Chatterjee.
 */
class ConfigElement<T : Any> (var id: String, initialValue: T? = null) {
    val valueProperty = SimpleObjectProperty(initialValue)
    var value by valueProperty

    init {
        valueProperty.onChange {
            val configRepository = LetterBoxContext.getBean(ConfigRepository::class)
            configRepository?.markDirty()
        }
    }
}

@Suppress("UNCHECKED_CAST")
fun <T: Any> readConfigElement(id: String, initialValue: T? = null): T? {
    val configRepository = LetterBoxContext.getBean(ConfigRepository::class)!!
    val configElement: ConfigElement<T>

    if (configRepository.containsElement(id)) {
        configElement = configRepository[id] as ConfigElement<T>
    } else {
        configElement = ConfigElement(id, initialValue)
        configRepository[id] = configElement
    }
    return configElement.value
}

@Suppress("UNCHECKED_CAST")
fun <T: Any> writeConfigElement(id: String, value: T?) {
    val configRepository = LetterBoxContext.getBean(ConfigRepository::class)!!

    val configElement = if (configRepository.containsElement(id)) {
        configRepository[id] as ConfigElement<T>
    } else {
        ConfigElement(id)
    }
    configElement.value = value
    configRepository[id] = configElement
}
