package org.dizitart.letterbox.api.plugin
/**
 * @author Anindya Chatterjee.
 */
data class PluginDescriptor(
        val name: String,
        val authorInfo: AuthorInfo,
        val description: Description
)

data class AuthorInfo(
        val author: String,
        val vendor: String,
        val vendorUrl: String
)

data class Description(
        val text: String,
        val releaseNote: String,
        val license: String
)
