package org.dizitart.letterbox.api


/**
 *
 * @author Anindya Chatterjee
 */
interface ChangeAware {
    var onChange: (() -> Unit)?

    fun changed() {
        onChange?.invoke()
    }
}