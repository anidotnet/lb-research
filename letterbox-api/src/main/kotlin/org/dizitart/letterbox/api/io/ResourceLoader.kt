package org.dizitart.letterbox.api.io

import java.io.InputStream

/**
 *
 * @author Anindya Chatterjee
 */
class ResourceLoader(private val classLoader: ClassLoader) {
    fun loadResource(name: String): InputStream {
        return classLoader.getResourceAsStream(name)
    }
}