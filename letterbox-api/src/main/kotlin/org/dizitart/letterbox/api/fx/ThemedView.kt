package org.dizitart.letterbox.api.fx

import javafx.beans.property.SimpleBooleanProperty
import javafx.geometry.BoundingBox
import javafx.geometry.NodeOrientation
import javafx.geometry.Pos
import javafx.scene.Node
import javafx.scene.Parent
import javafx.scene.layout.HBox
import javafx.scene.layout.Priority
import javafx.scene.paint.Color
import javafx.stage.Screen
import mu.KotlinLogging
import org.dizitart.letterbox.api.config.readConfigElement
import org.dizitart.letterbox.api.config.writeConfigElement
import org.dizitart.letterbox.api.styles.LetterBoxGraphicsCatalog
import org.dizitart.letterbox.api.styles.LetterBoxStylesheet
import tornadofx.*


/**
 *
 * @author Anindya Chatterjee
 */
abstract class ThemedView(private val showClose: Boolean = true,
                          private val showMinimize: Boolean = true,
                          private val showMaximize: Boolean = true,
                          private val titleBarHeight: Double,
                          title: String? = null,
                          icon: Node? = null) : View(title, icon) {
    private val logger = KotlinLogging.logger {  }
    private var originalBox: BoundingBox? = null
    private var maximizedBox: BoundingBox? = null
    private var xOffset = 0.0
    private var yOffset = 0.0
    private lateinit var titleBar: HBox
    private var stagePrepared = false

    abstract val container: Parent
    abstract val windowName: String
    open val defaultMaximize = true
    open val titleBarExtensions: HBox = hbox {  }

    private val maximizedProperty = SimpleBooleanProperty(defaultMaximize)
    private var maximized by maximizedProperty
    private var shouldMaximize = false

    override val root = vbox {
        addClass(LetterBoxStylesheet.window)

        hbox {
            addClass(LetterBoxStylesheet.titleBar)
            titleBar = this
            minHeight = titleBarHeight
            maxHeight = titleBarHeight
            nodeOrientation = NodeOrientation.RIGHT_TO_LEFT
            alignment = Pos.CENTER_LEFT

            if (showClose) {
                button {
                    addClass(LetterBoxStylesheet.controlButton,
                            LetterBoxStylesheet.lbImageButton,
                            LetterBoxStylesheet.lbImageButtonNormal)
                    addImage(LetterBoxGraphicsCatalog.closeButton)

                    action {
                        closeView()
                    }
                }
            }

            if (showMaximize) {
                button {
                    addClass(LetterBoxStylesheet.controlButton,
                            LetterBoxStylesheet.lbImageButton,
                            LetterBoxStylesheet.lbImageButtonNormal)
                    addImage(LetterBoxGraphicsCatalog.maximizeButton)

                    action {
                        maximizeView()
                    }
                }
            }

            if (showMinimize) {
                button {
                    addClass(LetterBoxStylesheet.controlButton,
                            LetterBoxStylesheet.lbImageButton,
                            LetterBoxStylesheet.lbImageButtonNormal)
                    addImage(LetterBoxGraphicsCatalog.minimizeButton)

                    action {
                        minimizeView()
                    }
                }
            }

            setOnMousePressed { event ->
                xOffset = event.sceneX
                yOffset = event.sceneY
            }

            setOnMouseDragged { event ->
                currentStage?.x = event.screenX - xOffset
                currentStage?.y = event.screenY - yOffset
            }

            onDoubleClick {
                if (showMaximize) {
                    maximizeView()
                }
            }
        }
    }

    open fun closeView() {
        close()
    }

    open fun maximizeView() {
        val stage = currentStage
        if (stage != null) {
            if (!shouldMaximize) {
                this.originalBox = BoundingBox(stage.x, stage.y, stage.width, stage.height)
                val screens = Screen.getScreensForRectangle(stage.x,
                        stage.y,
                        stage.width,
                        stage.height)
                val primaryScreen = if (screens.isEmpty()) Screen.getPrimary() else screens[0]
                val bounds = primaryScreen.visualBounds
                maximizedBox = BoundingBox(bounds.minX,
                        bounds.minY,
                        bounds.width,
                        bounds.height)
                stage.x = maximizedBox?.minX!!
                stage.y = maximizedBox?.minY!!
                stage.width = maximizedBox?.width!!
                stage.height = maximizedBox?.height!!

                root.removeClass(LetterBoxStylesheet.window)
                root.addClass(LetterBoxStylesheet.windowMaximized)
                maximized = true
            } else {
                stage.x = originalBox?.minX!!
                stage.y = originalBox?.minY!!
                stage.width = originalBox?.width!!
                stage.height = originalBox?.height!!
                originalBox = null

                root.removeClass(LetterBoxStylesheet.windowMaximized)
                root.addClass(LetterBoxStylesheet.window)
                maximized = false
            }
            shouldMaximize = !shouldMaximize
        }
    }

    open fun minimizeView() {
        currentStage?.isIconified = true
    }

    override fun onBeforeShow() {
        super.onBeforeShow()

        root += container
        container.vgrow = Priority.ALWAYS
        titleBar += titleBarExtensions

        root.prefHeight = readConfigElement("$windowName.height", 770.0)!!
        root.prefWidth = readConfigElement("$windowName.width", 440.0)!!

        if (!stagePrepared) {
            prepareStage()
        }
    }

    override fun onUndock() {
        super.onUndock()
        writeConfigElement("$windowName.height", currentStage!!.height)
        writeConfigElement("$windowName.width", currentStage!!.width)
        writeConfigElement("$windowName.maximized", maximized)
    }

    protected open fun prepareStage() {
        maximized = readConfigElement("$windowName.maximized", defaultMaximize)!!

        currentStage!!.scene.fill = Color.TRANSPARENT
        ResizeHelper.addResizeListener(currentStage!!)

        if (maximized) {
            maximizeView()
        }
        stagePrepared = true
    }
}

abstract class ModalView(title: String? = null, icon: Node? = null)
    : ThemedView(true, false, false, 30.0, title, icon) {
    override val defaultMaximize = false
}

abstract class WindowView(title: String? = null, icon: Node? = null, titleBarHeight: Double = 30.0)
    : ThemedView(true, true, true, titleBarHeight, title, icon)