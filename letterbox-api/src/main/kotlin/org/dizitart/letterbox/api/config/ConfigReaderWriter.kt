package org.dizitart.letterbox.api.config

/**
 *
 * @author Anindya Chatterjee
 */
interface ConfigReaderWriter {
    fun read(): Map<String, ConfigElement<*>>
    fun write(config: Map<String, ConfigElement<*>>)
}