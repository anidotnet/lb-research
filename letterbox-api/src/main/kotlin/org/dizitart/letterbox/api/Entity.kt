package org.dizitart.letterbox.api

import java.io.Serializable


/**
 * @author Anindya Chatterjee.
 */
class ItemContext {
    private val contextMap = mutableMapOf<String, String>()

    operator fun get(key: String): Any? {
        return contextMap[key]
    }

    operator fun set(key: String, value: String) {
        contextMap[key] = value
    }
}

interface ContextAware {
    var itemContext: ItemContext
}

interface Identifiable<Id> {
    var id: Id
}

interface Entity<Id> : Serializable, Identifiable<Id>

inline infix fun <reified Id, reified T : Identifiable<Id>> MutableList<T>.update(item: T) : Boolean {
    if (this.containsId(item)) {
        val element = this.getById(item.id)
        element?.let {
            if (element != item) {
                this.remove(element)
                this.add(item)
                return true
            }
        }
    } else {
        this.add(item)
        return true
    }
    return false
}

inline infix fun <reified Id, reified T : Identifiable<Id>> List<T>.containsId(item: T): Boolean {
    return this.any { it == item }
}

inline infix fun <reified Id, reified T : Identifiable<Id>> List<T>.getById(id: Id): T? {
    return this.firstOrNull { it.id == id }
}