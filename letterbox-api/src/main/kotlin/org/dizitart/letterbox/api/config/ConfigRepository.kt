package org.dizitart.letterbox.api.config

/**
 * @author Anindya Chatterjee.
 */
interface ConfigRepository {
    fun containsElement(id: String): Boolean
    operator fun get(id: String): ConfigElement<*>?
    operator fun set(id: String, element: ConfigElement<*>)
    fun markDirty()
    fun load()
    fun save()
}