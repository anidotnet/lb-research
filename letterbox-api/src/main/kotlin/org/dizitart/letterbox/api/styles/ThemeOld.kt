package org.dizitart.letterbox.api.styles

import javafx.scene.control.ContentDisplay
import javafx.scene.control.OverrunStyle
import javafx.scene.effect.BlurType
import javafx.scene.effect.DropShadow
import javafx.scene.effect.InnerShadow
import javafx.scene.layout.BackgroundRepeat
import javafx.scene.layout.BorderStrokeStyle
import javafx.scene.layout.BorderStrokeStyle.NONE
import javafx.scene.layout.BorderStrokeStyle.SOLID
import javafx.scene.paint.*
import javafx.scene.text.FontSmoothingType
import javafx.scene.text.FontWeight
import tornadofx.*


/**
 *
 * @author Anindya Chatterjee
 */
abstract class BaseLetterBoxTheme2(private val palette: ColorPalette2,
                                  val name: String) : Stylesheet() {

    //todo: get rid of all standard class and standard color definition
    //todo: split styles in email, calendar etc.
    companion object {
        val treePane by cssclass()
        val listPane by cssclass()
        val detailsPane by cssclass()
        val newButton by cssclass()
        val dropdownButton by cssclass()
        val borderlessButton by cssclass()
        val switchButton by cssclass()
        val folderTree by cssclass()
        val countColumn by cssclass()
        val searchBox by cssclass()
        val statusPane by cssclass()
        val statusLabel by cssclass()
        val trackBackground by cssclass()
        val radioMenuItem by cssclass()
        val itemList by cssclass()
        val emailSummery by cssclass()
        val subjectLabelLarge by cssclass()
        val subjectLabel by cssclass()
        val smallText by cssclass()
        val dateLabel by cssclass()
        val recipientLabel by cssclass()
        val senderName by cssclass()
        val unreadIndicator by cssclass()
        val fromLabel by cssclass()
        val emailAddressLabel by cssclass()
        val largeEmailAddressLabel by cssclass()
        val addressPopup by cssclass()
        val contentLabel by cssclass()
        val selectableLabel by cssclass()
        val darkIcon by cssclass()
        val lightIcon by cssclass()
        val vScroll by cssclass()
        val noHScroll by cssclass()
        val recipientPane by cssclass()
        val decoratedEmailAddress by cssclass()
        val decoratedEmailAddressLabel by cssclass()
        val lineBreak by cssclass()
        val whiteBg by cssclass()
        val borderless by cssclass()
        val recipientTypeText by cssclass()
        val recipientPopup by cssclass()
        val statusProgress by cssclass()
        val accountTile by cssclass()
        val caption by cssclass()
        val logo by cssclass()
        val lbMenu by cssclass()
        val tabItem by cssclass()
    }

    init {
        s(button and focused,
                toggleButton and focused,
                radioButton and focused child radio,
                checkBox and focused child box,
                menuButton and focused,
                choiceBox and focused,
                colorPicker and splitButton and focused child colorPickerLabel,
                comboBoxBase and focused,
                slider and focused contains thumb) {
            unsafe("-ui-background-color", raw("-ui-shadow-highlight-color, -ui-outer-border, -ui-inner-border, -ui-body-color"))
            backgroundInsets = multi(box(0.px, 0.px, (-1).px, 0.px), box(0.px), box(1.px), box(2.px))
            backgroundRadius = multi(box(3.px), box(3.px), box(2.px), box(1.px))
        }

        s(scrollPane and focused,
                splitPane and focused,
                listView and focused,
                treeView and focused,
                tableView and focused,
                treeTableView and focused,
                htmlEditor and focused) {
            unsafe("-ui-background-color", raw("-ui-box-border, -ui-control-inner-background"))
            backgroundInsets = multi(box(0.px), box(1.px))
            padding = box(1.px)
        }

        s(selectableLabel, selectableLabel contains scrollPane,
                selectableLabel contains viewport, selectableLabel contains content) {
            and(focused) {
                backgroundColor += palette.transparent
                backgroundRadius += box(0.px)
                borderColor += box(palette.transparent)
                borderWidth = multi(box(0.px), box(0.px))
                borderStyle += NONE
                backgroundInsets += box(0.px)
            }
            borderColor += box(palette.transparent)
            borderWidth += box(0.px)
            backgroundRadius += box(0.px)
            backgroundColor += palette.transparent
            borderStyle += NONE
            backgroundInsets += box(0.px)
        }

        treePane {
            backgroundColor += palette.color1
            borderStyle += NONE
        }

        listPane {
            backgroundColor += palette.color7
            borderStyle += NONE
        }

        detailsPane {
            backgroundColor += palette.color7
            borderStyle += NONE
        }

        splitPane {
            backgroundColor += palette.transparent
            backgroundInsets += box(0.px)
            padding = box(0.px)
        }

        splitPane child splitPaneDivider {
            padding = box(0.0.em, 0.1.em)
            backgroundColor += palette.color7
            borderStyle += NONE
        }

        s(newButton and hover contains imageView) {
            effect = InnerShadow(BlurType.GAUSSIAN, palette.color9, 7.0, 1.0, 1.0, 1.0)
        }

        s(newButton, newButton and focused) {
            borderColor += box(palette.transparent)
            borderWidth += box(0.px)
            backgroundRadius += box(0.px)
            backgroundColor += palette.transparent
        }

        s(dropdownButton child arrowButton) {
            padding = box(0.px)
        }

        s(dropdownButton child arrowButton child arrow) {
            padding = box(0.px)
        }

        darkIcon {
            and (hover) {
                effect = InnerShadow(BlurType.GAUSSIAN, palette.darkIconColor, 7.0, 1.0, 1.0, 1.0)
            }
        }

        lightIcon {
            and (hover) {
                effect = InnerShadow(BlurType.GAUSSIAN, palette.lightIconColor, 7.0, 1.0, 1.0, 1.0)
            }
        }

        borderlessButton {
            and(hover) {
                backgroundColor += palette.transparent
                backgroundRadius += box(0.px)
                borderColor += box(palette.transparent)
                borderWidth = multi(box(0.px), box(0.px))
                borderStyle += NONE
            }

            and(focused) {
                borderColor += box(palette.transparent)
                borderWidth = multi(box(0.px))
                backgroundRadius += box(0.px)
                backgroundColor += palette.transparent
            }

            borderColor += box(palette.transparent)
            borderWidth += box(0.px)
            backgroundRadius += box(0.px)
            backgroundColor += palette.transparent
        }

        switchButton {
            borderColor += box(palette.transparent)
            borderWidth += box(0.px)
            backgroundRadius += box(0.px)
            backgroundColor += palette.transparent

            and(hover) {
                backgroundColor += palette.transparent
                backgroundRadius += box(0.px)
                borderColor += box(palette.transparent)
                borderWidth = multi(box(0.px), box(0.px))
                borderStyle += NONE
            }

            and(focused) {
                borderColor += box(palette.transparent)
                borderWidth = multi(box(0.px))
                backgroundRadius += box(0.px)
                backgroundColor += palette.transparent
            }
        }

        s(folderTree, folderTree and focused) {
            unsafe("-ui-table-cell-border-color", raw("transparent"))
            unsafe("-ui-selection-bar", raw("transparent"))
            unsafe("-ui-selection-bar-non-focused", raw("transparent"))
            backgroundColor += palette.transparent
        }

        folderTree contains columnHeaderBackground {
            visibility = FXVisibility.HIDDEN
            padding = box((-1).em)
        }

        folderTree contains treeTableRowCell {
            backgroundColor += palette.color1
            textFill = palette.color7
            cellSize = 40.px
        }

        folderTree contains tableColumn {
            textFill = palette.color7
            textOverrun = OverrunStyle.CENTER_WORD_ELLIPSIS
        }

        folderTree contains countColumn {
            textFill = palette.color4
            fontWeight = FontWeight.BOLD
        }

        folderTree contains arrow {
            backgroundColor += palette.color7
        }

        s(noHScroll contains star and scrollBar and horizontal,
                noHScroll contains star and scrollBar and horizontal contains star and track,
                noHScroll contains star and scrollBar and horizontal contains star and trackBackground,
                noHScroll contains star and scrollBar and horizontal contains star and thumb,
                noHScroll contains star and scrollBar and horizontal contains star and incrementButton,
                noHScroll contains star and scrollBar and horizontal contains star and decrementButton,
                noHScroll contains star and scrollBar and horizontal contains star and incrementArrow,
                noHScroll contains star and scrollBar and horizontal contains star and decrementArrow) {
            backgroundColor += palette.transparent
            backgroundRadius += box(0.px)
            backgroundInsets += box(0.px)
            padding = box(0.px)
            unsafe("-ui-shape", raw("null"))
        }

        s(vScroll contains star and scrollBar and vertical,
                vScroll contains star and scrollBar and vertical contains star and track,
                vScroll contains star and scrollBar and vertical contains star and trackBackground,
                vScroll contains star and scrollBar and vertical contains star and incrementArrow,
                vScroll contains star and scrollBar and vertical contains star and decrementArrow) {
            unsafe("-ui-background-color", raw("null"))
            backgroundRadius += box(0.px)
            backgroundInsets += box(0.px)
            padding = box(0.px)
            unsafe("-ui-shape", raw("null"))
        }

        s(vScroll contains star and scrollBar and vertical contains star and incrementButton,
                vScroll contains star and scrollBar and vertical contains star and decrementButton) {
            backgroundColor += palette.transparent
            borderColor += box(palette.transparent)
            backgroundRadius += box(0.px)
            unsafe("-ui-shape", raw("null"))
            unsafe("-ui-effect", raw("null"))
        }

        vScroll contains star and scrollBar and vertical contains star and thumb {
            borderColor += box(palette.transparent)
            backgroundColor += palette.transparent
            backgroundRadius += box(0.px)
        }

        s(vScroll contains star and scrollBar and vertical and hover contains star and thumb,
                vScroll contains star and scrollBar and vertical contains star and track and hover contains star and thumb,
                folderTree contains star and scrollBar and vertical contains star and thumb and hover) {
            backgroundColor += palette.color11
        }

        vScroll contains star and scrollBar and vertical contains star and thumb and pressed {
            backgroundColor += palette.color2
        }

        searchBox {
            backgroundColor += palette.transparent
            backgroundRadius += box(2.px)
            padding = box(0.5.em, 0.5.em, 0.5.em, 2.5.em)
            borderColor += box(palette.color11)
            borderWidth += box(0.px, 0.px, 2.px, 0.px)
            promptTextFill = palette.color12
            highlightFill = palette.color2
//            backgroundImage += url("images/search.png")
            unsafe("-ui-background-size", raw("25px"))
            backgroundRepeat += Pair(BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT)
            unsafe("-ui-background-position", raw("10px center"))
            borderInsets += box(0.px, 5.px, 0.px, 5.px)
            backgroundInsets += box(0.px, 5.px, 0.px, 5.px)

            and(focused) {
                backgroundColor += palette.transparent
                backgroundRadius += box(2.0.px)
                padding = box(0.5.em)
                borderColor += box(palette.color13)
                borderWidth += box(0.px, 0.px, 2.px, 0.px)
                promptTextFill = palette.transparent
                highlightFill = palette.color2
                unsafe("-ui-background-image", raw("null"))
            }
        }

        searchBox child star child text {
            unsafe("-ui-effect", raw("null"))
            fill = palette.color8
        }

        statusPane {
            borderStyle += SOLID
            borderColor += box(palette.color14)
            borderWidth += box(1.px, 0.px, 0.px, 0.px)
        }

        statusLabel {
            fontSize = 12.px
        }

        separator contains star and line {
            borderStyle = multi(NONE, NONE, SOLID, NONE)
            borderWidth += box(0.px, 0.px, 0.5.px, 0.px)
            backgroundColor += RadialGradient(0.0, 0.0,
                    0.5, 0.5,
                    0.5, true,
                    CycleMethod.NO_CYCLE,
                    Stop(0.0, palette.color10), Stop(1.0, palette.transparent))
        }

        contextMenu {
            backgroundColor += palette.color1
            textFill = palette.color7
            effect = DropShadow(BlurType.THREE_PASS_BOX, palette.color6, 4.0, 0.0, 0.0, 0.0)
        }

        menuItem child label {
            textFill = palette.color7
            prefWidth = 150.px
        }

        s(menuItem and focused, menuItem and focused child label) {
            backgroundColor += palette.color2
            textFill = palette.color7
        }

        menuItem contains graphicContainer {
            padding = box(0.em, 1.5.em, 0.em, 0.em)
        }

        radioMenuItem and checked child leftContainer child radio {
            backgroundColor += Color.WHITE
        }

        s(itemList, itemList and focused, itemList and selected) {
            unsafe("-ui-control-inner-background", raw("transparent"))
            unsafe("-ui-control-inner-background-alt", raw("-ui-control-inner-background"))
            backgroundColor += palette.transparent
            fitToWidth = true
            unsafe("-ui-effect", raw("null"))
            unsafe("-ui-selection-bar", raw("transparent"))
            unsafe("-ui-selection-bar-non-focused", raw("transparent"))
            borderColor += box(RadialGradient(0.0, 0.0,
                    0.5, 0.5,
                    0.5, true,
                    CycleMethod.NO_CYCLE,
                    Stop(0.0, palette.color1), Stop(1.0, palette.transparent)))
            borderStyle = multi(SOLID, NONE, NONE, NONE)
            borderWidth += box(1.px, 0.px, 0.px, 0.px)
        }

        itemList contains listCell {
            padding = box(0.px)
            backgroundColor += palette.transparent
        }

        s(itemList contains listCell and filled and selected and focused,
                itemList contains listCell and filled and selected) {
            backgroundColor += palette.transparent
            unsafe("-ui-effect", raw("null"))
        }

        itemList contains listCell and filled and hover {
            backgroundColor += palette.transparent
            unsafe("-ui-effect", raw("null"))
        }

        s(itemList contains listCell and filled and selected and focused contains emailSummery,
                itemList contains listCell and filled and selected contains emailSummery) {
            backgroundColor += palette.color3
        }

        s(itemList contains listCell and filled and selected and focused contains emailSummery contains star,
                itemList contains listCell and filled and selected contains emailSummery contains star) {
            textFill = palette.color7
        }

        emailSummery {
            backgroundColor += palette.color7
            borderStyle = multi(NONE, NONE, SOLID, NONE)
            borderWidth += box(0.px, 0.px, 1.px, 0.px)
            borderColor += box(RadialGradient(0.0, 0.0,
                    0.5, 0.5,
                    0.5, true,
                    CycleMethod.NO_CYCLE,
                    Stop(0.0, palette.color1), Stop(1.0, palette.transparent)))
        }

        subjectLabel {
            textFill = palette.color8
            fontSmoothingType = FontSmoothingType.GRAY
            fontSize = 13.px
        }

        subjectLabelLarge {
            minHeight = 18.px
            textFill = palette.color8
            fontSize = 18.px
            fontWeight = FontWeight.SEMI_BOLD
            fontFamily = "Open Sans"
        }

        smallText {
            fontSize = 11.px
        }

        dateLabel {
            textFill = palette.color8
            fontSize = 10.px
            fontWeight = FontWeight.SEMI_BOLD
            fontFamily = "Open Sans"
        }

        recipientLabel and hover {
            textFill = palette.color15
            underline = true
        }

        senderName {
            fontSize = 14.px
        }

        unreadIndicator {
            backgroundColor += palette.color2
        }

        fromLabel {
            textFill = palette.color16
            fontSmoothingType = FontSmoothingType.GRAY
            fontSize = 11.px
            fontWeight = FontWeight.SEMI_BOLD
        }

        largeEmailAddressLabel {
            textFill = palette.color16
            fontSmoothingType = FontSmoothingType.GRAY
            fontSize = 16.px
            fontWeight = FontWeight.SEMI_BOLD
        }

        emailAddressLabel {
            textFill = palette.color12
            fontSmoothingType = FontSmoothingType.GRAY
            fontSize = 12.px
        }

        addressPopup {
            backgroundColor += palette.color7
            borderStyle += SOLID
            borderColor += box(palette.color11)
            borderWidth += box(1.px)
            effect = DropShadow(BlurType.THREE_PASS_BOX, palette.color12, 5.0, 0.0, 0.0, 5.0)
        }

        recipientPane {
            borderStyle += NONE
            borderWidth += box(0.px)
        }

        s(recipientPane contains content) {
            borderStyle += NONE
            borderWidth += box(0.px)
        }

        s(recipientPane contains title) {
            borderStyle += NONE
            borderWidth += box(0.px)
            backgroundColor += palette.transparent
            padding = box(0.px)
        }

        s(recipientPane contains arrow, recipientPane contains arrowButton) {
            backgroundColor += palette.transparent
            padding = box(0.px)
            unsafe("-ui-shape", raw("null"))
            unsafe("-ui-effect", raw("null"))
        }

        contentLabel {
            textFill = palette.color8
            fontSmoothingType = FontSmoothingType.GRAY
            fontSize = 11.px
        }

        decoratedEmailAddress {
            borderWidth += box(1.px)
            borderStyle += SOLID
            borderColor += box(palette.color13)
        }

        decoratedEmailAddressLabel {
            fontWeight = FontWeight.BOLD
            fontSize = 12.px
            padding = box(5.px, 5.px, 1.px, 5.px)
        }

        lineBreak {
            borderWidth += box(0.px, 0.px, 1.px, 0.px)
            borderStyle += BorderStrokeStyle.SOLID
            borderColor += box(Color.SLATEGRAY)
        }

        whiteBg {
            backgroundColor += palette.color7
            and(focused) {
                backgroundColor += palette.color7
            }
        }

        s(borderless, borderless and focused) {
            borderStyle += NONE
            borderWidth += box(0.px)
            borderColor += box(palette.transparent)
        }

        recipientTypeText {
            fontWeight = FontWeight.SEMI_BOLD
            fontSize = 14.px
            padding = box(10.px, 0.px, 0.px, 0.px)
        }

        s(recipientPopup, recipientPopup and focused) {
            borderStyle += SOLID
            borderColor += box(palette.color11)
            borderWidth += box(0.px, 0.px, 0.px, 0.px)
            borderRadius += box(1.px)
            backgroundColor += palette.color7
            effect = DropShadow(BlurType.THREE_PASS_BOX, palette.color12, 5.0, 0.0, 0.0, 5.0)
        }

        statusProgress {
            padding = box(5.0.px, 10.0.px)
        }

        statusProgress child track {
            backgroundColor += Color.TRANSPARENT
        }

        statusProgress child bar {
            backgroundInsets += box(1.px)
            padding = box(3.px)
            backgroundRadius += box(1.px)
            backgroundColor += LinearGradient.valueOf("linear-gradient(to bottom, #0096C9, #0096C9)")
        }

        accountTile {
            backgroundColor += Color.WHITE
            and(hover, focused, selected) {
                effect = DropShadow(BlurType.THREE_PASS_BOX, palette.color6, 5.0, 0.0, 5.0, 5.0)
            }
        }

        caption {
            fontSize = 30.px
            textFill = Color.LIGHTGRAY
            fontFamily = "Quicksand"
            fontWeight = FontWeight.BOLD
        }

        logo {
            textFill = Color.DEEPSKYBLUE
            fontSize = 30.px
            fontWeight = FontWeight.BOLD
            fontFamily = "Nunito"
        }

        lbMenu {
            and(focused, selected) {
                backgroundColor += palette.transparent
                backgroundRadius += box(0.px)
                borderColor += box(palette.transparent)
                borderWidth = multi(box(0.px), box(0.px))
                borderStyle += NONE
            }
            borderColor += box(palette.transparent)
            borderWidth += box(0.px)
            backgroundRadius += box(0.px)
            backgroundColor += palette.transparent
            borderStyle += NONE
        }

        tabItem {
            contentDisplay = ContentDisplay.TOP
        }
    }
}

interface ColorPalette2 {
    val color1: Color
    val color2: Color
    val color3: Color
    val color4: Color
    val color5: Color
    val color6: Color
    val color7: Color
    val color8: Color

    val color9: Color
    val color10: Color
    val color11: Color
    val color12: Color
    val color13: Color
    val color14: Color
    val color15: Color
    val color16: Color
    val transparent: Color

    val darkIconColor: Color
    val lightIconColor: Color
}