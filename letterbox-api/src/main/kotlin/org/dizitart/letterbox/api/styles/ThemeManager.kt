package org.dizitart.letterbox.api.styles

import org.dizitart.letterbox.api.config.Default
import org.dizitart.letterbox.api.config.readConfigElement
import org.dizitart.letterbox.api.config.writeConfigElement
import org.dizitart.letterbox.common.di.LetterBoxContext
import tornadofx.*
import java.util.*
import kotlin.reflect.KClass

/**
 * @author Anindya Chatterjee.
 */
class ThemeManager {
    private val graphicsDefinitions = mutableSetOf<Class<out GraphicsCatalog>>()
    private val stylesheetDefinitions = mutableSetOf<Class<out PaletteAwareStylesheet>>()
    private val fontDefinitions = mutableSetOf<Class<out LetterBoxFont>>()

    fun <T : PaletteAwareStylesheet> installStylesheetDefinition(definition: Class<T>) = stylesheetDefinitions.add(definition)

    fun getStylesheetDefinitions(): Set<Class<out PaletteAwareStylesheet>> = stylesheetDefinitions.toSet()

    fun <T : GraphicsCatalog> installGraphicsDefinition(definition: Class<T>) = graphicsDefinitions.add(definition)

    fun getGraphicsDefinitions(): Set<Class<out GraphicsCatalog>> = graphicsDefinitions.toSet()

    fun <T : LetterBoxFont> installFontDefinition(definition: Class<T>) = fontDefinitions.add(definition)

    fun getFontDefinitions(): Set<Class<out LetterBoxFont>> = fontDefinitions.toSet()

    fun loadThemes() {
        // register definition of letterbox core stylesheet and graphics
        installStylesheetDefinition(LetterBoxStylesheet::class.java)
        installGraphicsDefinition(LetterBoxGraphicsCatalog::class.java)
        installFontDefinition(LetterBoxFont::class.java)

        loadStylesheet()
        loadGraphics()
        loadFonts()
    }

    fun saveStylesheet(stylesheet: Class<out PaletteAwareStylesheet>) {
        saveResource(getStylesheetDefinitions(), stylesheet)
    }

    fun saveGraphics(graphicsType: Class<out GraphicsCatalog>) {
        saveResource(getGraphicsDefinitions(), graphicsType)
    }

    fun saveFont(fontType: Class<out LetterBoxFont>) {
        saveResource(getFontDefinitions(), fontType)
    }

    private fun loadStylesheet() {
        loadResource(getStylesheetDefinitions()) {
            importStylesheet(this)
        }
    }

    private fun loadGraphics() {
        loadResource(getGraphicsDefinitions()) {
            loadCatalog()
        }
    }

    private fun loadFonts() {
        loadResource(getFontDefinitions()) {
            loadFont()
        }
    }

    private inline fun <reified T: Any> saveResource(definitions: Set<Class<out T>>, resourceType: Class<out T>) {
        definitions.forEach { definition ->
            if (definition.isAssignableFrom(resourceType)) {
                writeConfigElement(definition.name, resourceType.name)
                return
            }
        }
    }

    private inline fun <reified T: Any> loadResource(definitions: Set<Class<out T>>, crossinline action: T.() -> Unit) {
        definitions.forEach { definition ->
            val resources = LetterBoxContext.getBeans(definition.kotlin)

            if (resources.isNotEmpty()) {
                resources.forEach { resource ->
                    if (resource::class.java.hasAnnotation(Default::class)) {
                        resource.action()
                    }
                }

                val chosenType = readConfigElement<String>(definition.name) ?: return@forEach
                if (chosenType.isEmpty()) return@forEach

                resources.forEach { resource ->
                    if (chosenType == resource.javaClass.name) {
                        resource.action()
                    }
                }
            }
        }
    }

    private fun <T : Stylesheet> importStylesheet(style: T) {
        val cssString = Base64.getEncoder().encodeToString(style.render().toByteArray())
        val url = StringBuilder("css://$cssString:64")
        val urlString = url.toString()
        if (urlString !in FX.stylesheets) FX.stylesheets.add(url.toString())
    }

    private fun <T : Annotation> Class<*>.hasAnnotation(clazz: KClass<T>) : Boolean {
        return this.getDeclaredAnnotationsByType(clazz.java).isNotEmpty()
    }
}