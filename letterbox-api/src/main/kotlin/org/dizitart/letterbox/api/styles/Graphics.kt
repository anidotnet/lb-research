package org.dizitart.letterbox.api.styles

import org.dizitart.letterbox.api.io.Resource
import org.dizitart.letterbox.api.io.ResourceLoader

/**
 *
 * @author Anindya Chatterjee
 */
abstract class Graphics : Resource {
    var graphicsLoader: ResourceLoader? = null
}

data class ButtonGraphics(
        val image: String,
        val hoverImage: String,
        val activeImage: String
) : Graphics()

data class NodeGraphics(
        val image: String
) : Graphics()