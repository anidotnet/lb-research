package org.dizitart.letterbox.api.styles

import javafx.scene.image.Image

/**
 *
 * @author Anindya Chatterjee
 */
interface IconSet {
    companion object {
        val SIZE_10 = 10.0
        val SIZE_12 = 12.0
        val SIZE_14 = 14.0
        val SIZE_18 = 18.0
        val SIZE_20 = 20.0
        val SIZE_24 = 24.0
        val SIZE_25 = 25.0
        val SIZE_30 = 30.0
    }

    val name: String

    val newItemImage: Image
    val emailButtonImage: Image
    val calendarButtonImage: Image
    val taskButtonImage: Image
    val notesButtonImage: Image
    val verticalMenuImage: Image
    val selectAllImage: Image
    val syncImage: Image
    val backImage: Image
    val rightImage: Image
    val filterImage: Image
    val noFilterImage: Image
    val unreadImage: Image
    val favoriteImage: Image
    val favoriteImageSmall: Image
    val favoriteImageLight: Image
    val notFavoriteImage: Image
    val dateImage: Image
    val subjectImage: Image
    val personImage: Image
    val sortImage: Image
    val ascendingImage: Image
    val menuImage: Image
    val settingsImage: Image
    val replyImage: Image
    val replyAllImage: Image
    val forwardImage: Image
    val labelOutlineImage: Image
    val blockImage: Image
    val infoImage: Image
    val trashImage: Image
    val warningImage: Image
    val attachmentImage: Image
    val saveAddressImage: Image
    val sendEmailImage: Image
    val emailAddressImage: Image
    val inboxFolderImage: Image
    val sentFolderImage: Image
    val draftFolderImage: Image
    val outboxFolderImage: Image
    val spamFolderImage: Image
    val trashFolderImage: Image
    val folderImage: Image
    val closeImage: Image
    val nextImage: Image
    val previousImage: Image
    val finishImage: Image
    val pluginImage: Image
    val accountImage: Image
}