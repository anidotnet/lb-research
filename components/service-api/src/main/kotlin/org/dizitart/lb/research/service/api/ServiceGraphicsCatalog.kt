package org.dizitart.lb.research.service.api

import org.dizitart.letterbox.api.styles.ButtonGraphics
import org.dizitart.letterbox.api.styles.GraphicsCatalog
import tornadofx.cssclass

/**
 * @author Anindya Chatterjee.
 */
abstract class ServiceGraphicsCatalog : GraphicsCatalog {
    companion object {
        val emailTabButton by cssclass()
        val calendarTabButton by cssclass()
    }

    abstract val emailTabButtonIcon: ButtonGraphics
    abstract val calendarTabButtonIcon: ButtonGraphics

    override fun loadCatalog() {
        this.registerElement(emailTabButton, emailTabButtonIcon)
        this.registerElement(calendarTabButton, calendarTabButtonIcon)
    }
}