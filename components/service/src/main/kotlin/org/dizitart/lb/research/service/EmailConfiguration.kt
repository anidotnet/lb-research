package org.dizitart.lb.research.service

import org.dizitart.letterbox.api.config.ConfigView
import org.dizitart.letterbox.api.config.EditableConfiguration
import tornadofx.checkbox
import tornadofx.vbox

/**
 * @author Anindya Chatterjee.
 */
class EmailConfiguration : EditableConfiguration {
    companion object {
        private const val ENABLE_EMAIL = "email.enable"
        private const val ENABLE_SERVER = "server.enable"
    }

    // add multiple config group
    override val groupHierarchy = arrayOf("General", "Email")

    // add different views
    override fun getView() = object : ConfigView() {
        override val root = vbox {
            checkbox("Enable Email") {
                selectedProperty().bindBidirectional(configElement(ENABLE_EMAIL))
            }

            checkbox("Enable Server") {
                selectedProperty().bindBidirectional(configElement(ENABLE_SERVER))
            }
        }
    }

    val enableEmail: Boolean? by configElement(ENABLE_EMAIL)
    val emailServer: String? by configElement(ENABLE_SERVER)
}

class CalendarConfiguration : EditableConfiguration {
    companion object {
        private const val ENABLE_CALENDAR = "calendar.enable"
    }

    override val groupHierarchy = arrayOf("General", "Calendar")

    override fun getView() = object : ConfigView() {
        override val root = vbox {
            checkbox("Enable Calendar") {
                selectedProperty().bindBidirectional(configElement(ENABLE_CALENDAR))
            }
        }
    }

    val enabled: Boolean? by configElement(ENABLE_CALENDAR)
}