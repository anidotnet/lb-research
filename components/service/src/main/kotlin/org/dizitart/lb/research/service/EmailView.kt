package org.dizitart.lb.research.service

import javafx.geometry.Pos
import javafx.scene.layout.Priority
import tornadofx.*

/**
 * @author Anindya Chatterjee.
 */
class EmailView : View() {
    override val root = vbox {
        label(text = "Hello Email")
        button("Click Me!") {
            setOnMouseClicked {
                println("Email View")
            }
        }
    }
}

class EmailSearchView : View() {
    override val root = hbox {
        hgrow = Priority.ALWAYS
        alignment = Pos.CENTER_LEFT
        textfield {
            hgrow = Priority.ALWAYS
            promptText = "Search Email"
        }
    }
}

class EmailActionView : View() {
    override val root = hbox {
        alignment = Pos.CENTER_LEFT
        button {
            hgrow = Priority.ALWAYS
            text = "Compose Email"
        }
    }
}


class CalendarView : View() {
    override val root = vbox {
        label(text = "Hello Calendar")
        button("Click Me!") {
            setOnMouseClicked {
                println("Calendar View")
            }
        }
    }
}

class CalendarSearchView : View() {
    override val root = hbox {
        hgrow = Priority.ALWAYS
        alignment = Pos.CENTER_LEFT
        textfield {
            hgrow = Priority.ALWAYS
            promptText = "Search Calendar"
        }
    }
}

class CalendarActionView : View() {
    override val root = hbox {
        alignment = Pos.CENTER_LEFT
        button {
            text = "Compose Event"
        }
    }
}