package org.dizitart.lb.research.service


import org.dizitart.lb.research.service.api.ServiceGraphicsCatalog
import org.dizitart.letterbox.api.appVersion
import org.dizitart.letterbox.api.plugin.*
import org.dizitart.letterbox.api.styles.ThemeManager
import org.dizitart.letterbox.common.di.BeanCatalog
import org.dizitart.letterbox.common.di.LetterBoxContext
import org.dizitart.letterbox.common.di.beans

/**
 *
 * @author Anindya Chatterjee
 */
@PluginManifest(uniqueId = "email-service", version = appVersion)
class ServiceManifest : PluginManifestEntry {
    override fun start() {
        val themeManager = LetterBoxContext.getBean(ThemeManager::class)
        themeManager?.installGraphicsDefinition(ServiceGraphicsCatalog::class.java)
    }

    override fun stop() {

    }

    override val beanCatalog: BeanCatalog
        get() = beans {
            singleton { EmailComponent() }
            singleton { CalendarComponent() }
            singleton { CalendarConfiguration() }
            singleton { EmailConfiguration() }
        }

    override val descriptor: PluginDescriptor
        get() {
            try {
                val authorInfo = AuthorInfo("Dizitart", "Dizitart", "ww.goog.e")
                val description = Description("Email Extension", "", "")

                return PluginDescriptor("LetterBox Email Extension", authorInfo, description)
            } catch (e: Exception) {
                throw RuntimeException(e)
            }
        }
}