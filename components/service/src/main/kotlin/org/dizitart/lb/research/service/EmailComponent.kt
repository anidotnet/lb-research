package org.dizitart.lb.research.service

import javafx.scene.control.MenuItem
import org.dizitart.lb.research.service.api.ServiceGraphicsCatalog
import org.dizitart.letterbox.api.ComponentProvider
import tornadofx.*

/**
 * @author Anindya Chatterjee.
 */


//https://www.flaticon.com/packs/basic-icons
class EmailComponent : ComponentProvider {
    override val tabGraphics = ServiceGraphicsCatalog.emailTabButton
    override val name: String = "EmailComponent"
    override val enabled: Boolean = true
    override val ordinal: Int = 0
    override val pageView: View = find(EmailView::class)
    override val searchView: View = find(EmailSearchView::class)
    override val actionView: View = find(EmailActionView::class)
    override val menu: Set<MenuItem>
        get() = setOf(
                MenuItem("New Email").apply {
                    action {
                        println("New Email Clicked!")
                    }
                },
                MenuItem("Delete Email").apply {
                    action {
                        println("Delete Email Clicked")
                    }
                }
        )


    override fun init() {

    }
}

class CalendarComponent : ComponentProvider {
    override val tabGraphics = ServiceGraphicsCatalog.calendarTabButton
    override val menu: Set<MenuItem> = setOf()
    override val name: String = "CalendarComponent"
    override val enabled: Boolean = true
    override val ordinal: Int = 1
    override val pageView: View = find(CalendarView::class)
    override val searchView: View = find(CalendarSearchView::class)
    override val actionView: View = find(CalendarActionView::class)

    override fun init() {

    }
}