package org.dizitart.letterbox.core.plugin

/**
 * @author Anindya Chatterjee.
 */
open class PluginException : Exception {

    constructor() : super() {}

    constructor(message: String) : super(message) {}

    constructor(cause: Throwable) : super(cause) {}

    constructor(message: String, cause: Throwable) : super(message, cause) {}
}

class CyclicDependencyException
    : PluginException("Cyclic dependencies")

class DependenciesNotFoundException(val dependencies: List<String>)
    : PluginException("Dependencies '$dependencies' not found")

class DependenciesWrongVersionException(val dependencies: List<WrongDependencyVersion>)
    : PluginException("Dependencies '$dependencies' have wrong version")