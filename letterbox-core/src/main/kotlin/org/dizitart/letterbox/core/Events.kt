package org.dizitart.letterbox.core

import org.dizitart.letterbox.api.ComponentProvider
import tornadofx.FXEvent

/**
 *
 * @author Anindya Chatterjee
 */
data class ProviderChangeEvent(val provider: ComponentProvider) : FXEvent()