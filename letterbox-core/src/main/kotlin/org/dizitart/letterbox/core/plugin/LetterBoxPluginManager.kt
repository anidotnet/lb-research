package org.dizitart.letterbox.core.plugin

import mu.KotlinLogging
import org.dizitart.letterbox.api.plugin.PluginManifestEntry
import java.nio.file.Files
import java.nio.file.Path

/**
 * @author Anindya Chatterjee.
 */
class LetterBoxPluginManager(private val pluginInjector: PluginInjector,
                             override val pluginStateManager: PluginStateManager) : PluginManager {

    private val logger = KotlinLogging.logger {  }
    private val pluginFinder = PluginFinder()
    private val pluginLoader = PluginLoader()
    private val pluginResolver = PluginResolver()

    private val pluginDirs = mutableSetOf<Path>()
    private val plugins = mutableMapOf<String, PluginInfo>()

    override fun addPath(path: Path) {
        pluginDirs.add(path)
    }

    override fun loadPlugins() {
        val pluginsMetadata = loadPluginsMetadata()
        logger.debug { "Plugin metadata has been loaded" }

        val enabledPlugins = findEnabledPlugins(pluginsMetadata)
        logger.debug { "Metadata of enabled plugins has been processed" }

        val resolvedPlugins: Map<String, PluginMeta>
        try {
            resolvedPlugins = resolvePlugins(enabledPlugins)
        } catch (e: PluginException) {
            logger.error(e) { "Error resolving plugins dependencies" }
            return
        }
        logger.debug { "Plugins dependencies has been resolved" }

        if (resolvedPlugins.isEmpty()) return

        loadPluginInfo(resolvedPlugins)
        logger.debug { "Plugins has been loaded successfully" }

        injectPlugins()
        logger.debug { "Plugins has been injected into application itemContext" }
    }

    override fun startPlugins() {
        if (plugins.isNotEmpty()) {
            plugins.forEach { id, pluginInfo ->
                logger.debug { "Starting plugin $id" }
                val manifest = pluginInfo.pluginManifest
                try {
                    manifest.start()
                } catch (t: Throwable) {
                    logger.error(t) { "Unable to start plugin ${manifest.descriptor.name}" }
                    throw t
                }
            }
        }
    }

    override fun stopPlugins() {
        plugins.forEach { id, pluginInfo ->
            logger.debug { "Stopping plugin $id" }
            val manifest = pluginInfo.pluginManifest

            try {
                manifest.stop()
                val classLoader = pluginInfo.classLoader as PluginClassLoader
                classLoader.close()
            } catch(t: Throwable) {
                logger.error(t) { "Unable to stop plugin ${manifest.descriptor.name}" }
                throw t
            }
        }
        plugins.clear()
        pluginDirs.clear()
    }

    override fun getPluginClassLoader(pluginId: String): PluginClassLoader? {
        val pluginInfo = plugins[pluginId] ?: return null
        return pluginInfo.classLoader as PluginClassLoader
    }

    private fun loadPluginsMetadata(): List<PluginMeta> {
        val metadata = mutableListOf<PluginMeta>()
        pluginDirs.forEach { path ->
            if (Files.notExists(path) || !Files.isDirectory(path)) {
                return@forEach
            }

            val pluginPaths = pluginFinder.findPlugins(path)
            if (pluginPaths.isEmpty()) return@forEach

            for (pluginPath in pluginPaths) {
                try {
                    val pluginMetadata = pluginLoader.loadPluginMetadata(pluginPath)
                    metadata.add(pluginMetadata)
                } catch (e: Exception) {
                    logger.error(e) { "Error while loading plugin metadata from $pluginPath" }
                }
            }
        }
        return metadata
    }

    private fun resolvePlugins(plugins: Map<String, PluginMeta>) : Map<String, PluginMeta> {
        val result = pluginResolver.resolvePlugins(plugins)
        val resolvedPlugins = mutableMapOf<String, PluginMeta>()

        if (result.hasCyclicDependency()) {
            throw CyclicDependencyException()
        }

        val notFoundDependencies = result.getNotFoundDependencies()
        if (!notFoundDependencies.isEmpty()) {
            throw DependenciesNotFoundException(notFoundDependencies)
        }

        val wrongDependencies = result.getWrongVersionDependencies()
        if (!wrongDependencies.isEmpty()) {
            throw DependenciesWrongVersionException(wrongDependencies)
        }

        val sortedPlugins = result.getSortedPlugins() ?: return resolvedPlugins

        for (pluginId in sortedPlugins) {
            resolvedPlugins[pluginId] = plugins[pluginId]!!
        }

        return resolvedPlugins
    }

    private fun findEnabledPlugins(pluginsMetadata: List<PluginMeta>) : Map<String, PluginMeta> {
        val unresolvedPlugins = pluginsMetadata.associateBy { it.pluginId }
        val enabledPlugins = mutableMapOf<String, PluginMeta>()
        unresolvedPlugins.forEach { pluginId, pluginMeta ->
            if (isEnabled(pluginId)) {
                enabledPlugins[pluginId] = pluginMeta
            }
        }
        return enabledPlugins
    }

    private fun isEnabled(pluginId: String) : Boolean {
        val pluginState = pluginState(pluginId) ?: return false
        return pluginState.enabled
    }

    private fun pluginState(pluginId: String): PluginState? {
        return pluginStateManager.getPluginState(pluginId)
    }

    private fun loadPluginInfo(resolvedPlugins: Map<String, PluginMeta>) {
        resolvedPlugins.forEach { pluginId, pluginMeta ->
            val classLoader = PluginClassLoader(javaClass.classLoader, this, pluginMeta)
            val pluginManifest = findPluginManifest(classLoader, pluginMeta) ?: return@forEach

            val pluginInfo = PluginInfo(
                    pluginId = pluginId,
                    pluginMeta = pluginMeta,
                    pluginManifest = pluginManifest,
                    classLoader = classLoader
            )
            plugins[pluginId] = pluginInfo
        }
    }

    private fun findPluginManifest(classLoader: PluginClassLoader, pluginMeta: PluginMeta): PluginManifestEntry? {
        val manifestEntryType = pluginMeta.manifestType
        try {
            val manifestEntry = Class.forName(manifestEntryType, true, classLoader)
            val ctor = manifestEntry.getConstructor()
            val manifest = ctor.newInstance()
            if (manifest is PluginManifestEntry) {
                return manifest
            }
            throw IllegalStateException("$manifestEntry is not of type PluginManifestEntry")
        } catch (e: Exception) {
            logger.error(e) { "$pluginMeta could not be loaded from for ${pluginMeta.pluginId} from ${pluginMeta.path}" }
            return null
        }
    }

    private fun injectPlugins() {
        plugins.forEach(pluginInjector::inject)
    }
}