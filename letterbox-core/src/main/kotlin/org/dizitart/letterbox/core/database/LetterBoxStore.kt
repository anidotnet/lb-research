package org.dizitart.letterbox.core.database

import org.dizitart.kno2.nitrite
import org.dizitart.letterbox.common.APP_DATA_DIR
import org.dizitart.letterbox.common.beforeTerminate
import org.dizitart.no2.Nitrite
import org.dizitart.no2.NitriteCollection
import org.dizitart.no2.objects.ObjectRepository
import java.io.File
import java.nio.file.Files
import java.nio.file.Paths

/**
 *
 * @author Anindya Chatterjee
 */
object LetterBoxStore {
    private lateinit var db: Nitrite

    fun init() {
        db = nitrite {
            file = storeFile()
            disableShutdownHook = true
        }

        beforeTerminate(Int.MAX_VALUE) {
            if (!db.isClosed) {
                db.close()
            }
        }
    }

    @Suppress("UNCHECKED_CAST")
    fun <T> repository(clazz: Class<T>) : ObjectRepository<T> {
        return db.getRepository(clazz)
    }

    fun collection(name: String): NitriteCollection {
        return db.getCollection(name)
    }

    private fun storeFile() : File {
        val storeDir = APP_DATA_DIR + File.separator + "data"

        if (!Files.exists(Paths.get(storeDir))) {
            Files.createDirectory(Paths.get(storeDir))
        }

        return File(storeDir + File.separator + "letterbox.db")
    }

    fun save() {
        db.commit()
    }
}