package org.dizitart.letterbox.core.plugin

import com.github.zafarkhaja.semver.Version
import java.nio.file.Files
import java.nio.file.Path
import java.util.regex.Pattern

/**
 * @author Anindya Chatterjee.
 */
class PluginIndexReader(private val indexFilePath: Path) {
    private val comment = Pattern.compile("#.*")
    private val whitespace = Pattern.compile("\\s+")
    private val manifest = "manifest:"
    private val id = "id:"
    private val dependency = "dependency:"
    private val version = "version:"

    private var _manifestType = ""
    private var _pluginId = ""
    private var _pluginVersion = ""
    private val _dependencies = mutableSetOf<PluginDependency>()

    fun read() {
        if (Files.isReadable(indexFilePath)) {
            val reader = Files.newBufferedReader(indexFilePath)
            var line = reader.readLine()
            while (line != null) {
                line = comment.matcher(line).replaceFirst("")
                line = whitespace.matcher(line).replaceAll("")
                if (line.isNotEmpty()) {
                    when {
                        line.startsWith(manifest) -> {
                            if (_manifestType.isNotEmpty()) {
                                throw PluginException("Invalid plugin index file, multiple plugin manifests found")
                            }
                            _manifestType = line.removePrefix(manifest)
                        }
                        line.startsWith(id) -> {
                            if (_pluginId.isNotEmpty()) {
                                throw PluginException("Invalid plugin index file, multiple plugin id found")
                            }
                            _pluginId = line.removePrefix(id)
                        }
                        line.startsWith(dependency) -> {
                            val dep = line.removePrefix(dependency)
                            val split = dep.split(":")
                            if (split.size == 2) {
                                val pluginDependency = PluginDependency(split[0], split[1])
                                _dependencies.add(pluginDependency)
                            } else {
                                throw PluginException("Invalid plugin index file, invalid plugin dependency - $dep")
                            }
                        }
                        line.startsWith(version) -> {
                            if (_pluginVersion.isNotEmpty()) {
                                throw PluginException("Invalid plugin index file, multiple plugin version found")
                            }
                            _pluginVersion = line.removePrefix(version)
                        }
                    }
                }
                line = reader.readLine()
            }

            reader.close()
        }
    }

    val manifestType: String get() {
        return _manifestType
    }

    val pluginId: String get() {
        return _pluginId
    }

    val pluginVersion: Version
        get() {
            return Version.valueOf(_pluginVersion)
        }

    val dependencies: Set<PluginDependency> get() {
        return _dependencies
    }
}