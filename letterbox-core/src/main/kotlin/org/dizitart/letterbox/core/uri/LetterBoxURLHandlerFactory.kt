package org.dizitart.letterbox.core.uri

import java.net.URLStreamHandler
import java.net.URLStreamHandlerFactory

/**
 *
 * @author Anindya Chatterjee
 */
class LetterBoxURLHandlerFactory : URLStreamHandlerFactory {
    override fun createURLStreamHandler(protocol: String): URLStreamHandler? {
        return when (protocol) {
            "css" -> sun.net.www.protocol.css.Handler()
            else -> null
        }
    }
}