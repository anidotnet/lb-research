package org.dizitart.letterbox.core.config

import org.dizitart.letterbox.api.config.EditableConfiguration
import org.dizitart.letterbox.common.di.LetterBoxContext

/**
 * @author Anindya Chatterjee.
 */
internal class ConfigGroupBuilder {

    fun createConfigGroup(): ConfigGroup {
        val root = ConfigGroup("Settings")
        val groupMap = mutableMapOf<String, ConfigGroup>()
        val configurations = LetterBoxContext.getBeans(EditableConfiguration::class)

        configurations
                .forEach { configuration ->
                    val hierarchy = configuration.groupHierarchy
                    for ((index, value) in hierarchy.withIndex()) {
                        // add children and find parent
                        if (index != 0) {
                            val parent = hierarchy[index - 1]
                            var parentGroup = groupMap[parent]
                            if (parentGroup == null) {
                                parentGroup = ConfigGroup(parent)
                                groupMap[parent] = parentGroup
                            }
                            val group = ConfigGroup(value)
                            groupMap[value] = group
                            parentGroup.children.add(group)
                        } else {
                            if (!groupMap.containsKey(value)) {
                                val group = ConfigGroup(value)
                                groupMap[value] = group
                                root.children.add(group)
                            }
                        }

                        if (index == hierarchy.size - 1) {
                            val group = groupMap[value]
                            group?.view = configuration.getView()
                        }
                    }
                }
        return root
    }
}