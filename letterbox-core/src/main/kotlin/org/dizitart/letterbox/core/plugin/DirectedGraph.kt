package org.dizitart.letterbox.core.plugin

import java.util.*

/**
 * See [Wikipedia](https://en.wikipedia.org/wiki/Directed_graph) for more information.
 *
 * @author Decebal Suiu
 */
class DirectedGraph<V> {

    /**
     * The implementation here is basically an adjacency list, but instead
     * of an array of lists, a Map is used to map each vertex to its list of
     * adjacent vertices.
     */
    private val neighbors = HashMap<V, MutableList<V>>()

    /**
     * True if graph is a dag (directed acyclic graph).
     */
    val isDag: Boolean
        get() = topologicalSort() != null

    /**
     * Add a vertex to the graph. Nothing happens if vertex is already in graph.
     */
    fun addVertex(vertex: V) {
        if (containsVertex(vertex)) {
            return
        }

        neighbors[vertex] = ArrayList()
    }

    /**
     * True if graph contains vertex.
     */
    private fun containsVertex(vertex: V): Boolean {
        return neighbors.containsKey(vertex)
    }

    fun removeVertex(vertex: V) {
        neighbors.remove(vertex)
    }

    /**
     * Add an edge to the graph; if either vertex does not exist, it's added.
     * This implementation allows the creation of multi-edges and self-loops.
     */
    fun addEdge(from: V, to: V) {
        addVertex(from)
        addVertex(to)
        neighbors[from]?.add(to)
    }

    /**
     * Remove an edge from the graph. Nothing happens if no such edge.
     * @throws [IllegalArgumentException] if either vertex doesn't exist.
     */
    fun removeEdge(from: V, to: V) {
        if (!containsVertex(from)) {
            throw IllegalArgumentException("Nonexistent vertex $from")
        }

        if (!containsVertex(to)) {
            throw IllegalArgumentException("Nonexistent vertex $to")
        }

        neighbors[from]?.remove(to)
    }

    fun getNeighbors(vertex: V): MutableList<V> {
        val list = neighbors[vertex]
        return if (containsVertex(vertex) && list != null) list else mutableListOf()
    }

    /**
     * Report (as a Map) the out-degree (the number of tail ends adjacent to a vertex) of each vertex.
     */
    fun outDegree(): Map<V, Int> {
        val result = HashMap<V, Int>()
        for (vertex in neighbors.keys) {
            result[vertex] = neighbors[vertex]?.size ?: 0
        }

        return result
    }

    /**
     * Report (as a Map) the in-degree (the number of head ends adjacent to a vertex) of each vertex.
     */
    private fun inDegree(): MutableMap<V, Int> {
        val result = HashMap<V, Int>()
        for (vertex in neighbors.keys) {
            result[vertex] = 0 // all in-degrees are 0
        }
        for (from in neighbors.keys) {
            val list = neighbors[from]
            if (list != null) {
                for (to in list) {
                    result[to] = result[to]?.plus(1) ?: 0 // increment in-degree
                }
            }
        }

        return result
    }

    /**
     * Report (as a List) the topological sort of the vertices; null for no such sort.
     * See [this](https://en.wikipedia.org/wiki/Topological_sorting) for more information.
     */
    private fun topologicalSort(): MutableList<V>? {
        val degree = inDegree()

        // determine all vertices with zero in-degree
        val zeroVertices = Stack<V>() // stack as good as any here
        for (v in degree.keys) {
            if (degree[v] == 0) {
                zeroVertices.push(v)
            }
        }

        // determine the topological order
        val result = ArrayList<V>()
        while (!zeroVertices.isEmpty()) {
            val vertex = zeroVertices.pop() // choose a vertex with zero in-degree
            result.add(vertex) // vertex 'v' is next in topological order
            // "remove" vertex 'v' by updating its neighbors
            val list = neighbors[vertex]
            if (list != null) {
                for (neighbor in list) {
                    degree[neighbor] = degree[neighbor]?.minus(1) ?: 0
                    // remember any vertices that now have zero in-degree
                    if (degree[neighbor] == 0) {
                        zeroVertices.push(neighbor)
                    }
                }
            }
        }

        // check that we have used the entire graph (if not, there was a cycle)
        return if (result.size != neighbors.size) {
            null
        } else result

    }

    /**
     * Report (as a List) the reverse topological sort of the vertices; null for no such sort.
     */
    fun reverseTopologicalSort(): List<V>? {
        val list = topologicalSort() ?: return null
        list.reverse()
        return list
    }

    /**
     * String representation of graph.
     */
    override fun toString(): String {
        val sb = StringBuffer()
        for (vertex in neighbors.keys) {
            sb.append("\n   " + vertex + " -> " + neighbors[vertex])
        }

        return sb.toString()
    }

}
