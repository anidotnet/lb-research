package org.dizitart.letterbox.core.boot

import org.dizitart.letterbox.common.di.LetterBoxContext
import org.dizitart.letterbox.core.ComponentManager

/**
 * @author Anindya Chatterjee.
 */
class LetterBoxStarter {
    private val splashView = SplashView()
    private val storeManager = StoreManager()

    fun showSplash(complete: () -> Unit) {
        // open letterbox store and check single instance
        storeManager.openLetterBoxStore()

        // init the startup tasks like start configBean, plugins, di context
        val task = StartupTask()

        // show the splash screen and show the main page via a callback
        // once the splash screen loadProgressBar reaches 100%
        splashView.show(task, complete)

        // run the startup tasks behind the splash screen
        // and keep sending notification to splash screen
        // about it's loadProgressBar
        Thread(task).start()
    }

    fun initializeProviders() {
        val componentManager = LetterBoxContext.getBean(ComponentManager::class)
        componentManager?.initializeProviders()
    }
}