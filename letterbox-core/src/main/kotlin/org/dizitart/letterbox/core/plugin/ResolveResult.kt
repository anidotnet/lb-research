package org.dizitart.letterbox.core.plugin

import java.util.*

/**
 * @author Anindya Chatterjee.
 */
class ResolveResult internal constructor(sortedPlugins: List<String>?) {
    private var cyclicDependency: Boolean = false
    private var notFoundDependencies = mutableListOf<String>()
    private var sortedPlugins = mutableListOf<String>()
    private var wrongVersionDependencies = mutableListOf<WrongDependencyVersion>()

    /**
     * Returns true is a cyclic dependency was detected.
     */
    fun hasCyclicDependency(): Boolean {
        return cyclicDependency
    }

    /**
     * Returns a list with dependencies required that were not found.
     */
    fun getNotFoundDependencies(): List<String> {
        return notFoundDependencies
    }

    /**
     * Returns a list with dependencies with wrong version.
     */
    fun getWrongVersionDependencies(): List<WrongDependencyVersion> {
        return wrongVersionDependencies
    }

    /**
     * Get the list of plugins in dependency sorted order.
     */
    fun getSortedPlugins(): List<String>? {
        return sortedPlugins
    }

    internal fun addNotFoundDependency(pluginId: String) {
        notFoundDependencies.add(pluginId)
    }

    internal fun addWrongDependencyVersion(wrongDependencyVersion: WrongDependencyVersion) {
        wrongVersionDependencies.add(wrongDependencyVersion)
    }

    init {
        if (sortedPlugins == null) {
            cyclicDependency = true
            this.sortedPlugins = mutableListOf()
        } else {
            this.sortedPlugins = ArrayList(sortedPlugins)
        }
        notFoundDependencies = ArrayList()
        wrongVersionDependencies = ArrayList()
    }
}