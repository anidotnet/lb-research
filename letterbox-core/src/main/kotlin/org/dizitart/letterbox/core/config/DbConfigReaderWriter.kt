package org.dizitart.letterbox.core.config

import org.dizitart.kno2.option
import org.dizitart.letterbox.api.config.ConfigElement
import org.dizitart.letterbox.api.config.ConfigReaderWriter
import org.dizitart.letterbox.core.database.LetterBoxStore
import org.dizitart.no2.Document
import org.dizitart.no2.IndexType
import org.dizitart.no2.UpdateOptions.updateOptions
import org.dizitart.no2.filters.Filters.eq

/**
 *
 * @author Anindya Chatterjee
 */
internal class DbConfigReaderWriter : ConfigReaderWriter {
    private val repository = LetterBoxStore.collection("config")

    init {
        if (!repository.hasIndex("key")) {
            repository.createIndex("key", option(IndexType.Unique))
        }
    }

    override fun read(): Map<String, ConfigElement<*>> {
        return repository.find().associate { document ->
            document.get("key") as String to fromDocument(document)
        }
    }

    override fun write(config: Map<String, ConfigElement<*>>) {
        config.values.forEach { element ->
            repository.update(eq("key", element.id),
                    toDocument(element), updateOptions(true))
        }
    }

    private fun fromDocument(document: Document): ConfigElement<*> {
        val key = document["key"] as String
        val value = document["value"]
        return ConfigElement(key, value)
    }

    private fun toDocument(configElement: ConfigElement<*>): Document {
        val key = configElement.id
        val value = configElement.value
        val document = Document()
        document["key"] = key
        document["value"] = value
        return document
    }
}

