package org.dizitart.letterbox.core.plugin

import com.github.zafarkhaja.semver.Version
import mu.KotlinLogging

/**
 * @author Anindya Chatterjee.
 */
internal class PluginResolver {
    private val logger = KotlinLogging.logger {  }
    private val dependenciesGraph = DirectedGraph<String>()
    private val dependentsGraph = DirectedGraph<String>()
    private var resolved: Boolean = false

    fun resolvePlugins(plugins: Map<String, PluginMeta>): ResolveResult {
        plugins.forEach { entry ->
            addPlugin(entry.value)
        }

        logger.debug { "Dependency graph $dependenciesGraph" }

        val sortedPlugins = dependentsGraph.reverseTopologicalSort()
        logger.debug { "Plugins order $sortedPlugins" }

        val result = ResolveResult(sortedPlugins)
        resolved = true

        if (sortedPlugins != null) { // no cyclic dependency
            // detect not found dependencies
            for (pluginId in sortedPlugins) {
                if (!plugins.containsKey(pluginId)) {
                    result.addNotFoundDependency(pluginId)
                }
            }
        }

        // check dependencies versions
        for (entry in plugins) {
            val pluginId = entry.key
            val existingVersion = entry.value.pluginVersion

            val dependents = getDependents(pluginId)
            while (!dependents.isEmpty()) {
                val dependentId = dependents.removeAt(0)
                val dependent = plugins[dependentId]
                val requiredVersion = getDependencyVersionSupport(dependent, pluginId)
                val ok = checkDependencyVersion(existingVersion, requiredVersion)
                if (!ok) {
                    result.addWrongDependencyVersion(WrongDependencyVersion(pluginId, dependentId, existingVersion, requiredVersion))
                }
            }
        }

        return result
    }

    private fun getDependencyVersionSupport(dependent: PluginMeta?, dependencyId: String): String {
        val dependencies = dependent?.dependencies
        if (dependencies != null) {
            for (dependency in dependencies) {
                if (dependencyId == dependency.pluginId) {
                    return dependency.versionSupport
                }
            }
        }

        throw IllegalStateException("Cannot find a dependency with id '$dependencyId' " +
                "for plugin '${dependent?.pluginId}'")
    }

    private fun checkResolved() {
        if (!resolved) {
            throw IllegalStateException("Call 'resolve' method first")
        }
    }

    private fun getDependents(pluginId: String): MutableList<String> {
        checkResolved()
        return dependentsGraph.getNeighbors(pluginId)
    }

    private fun checkDependencyVersion(existingVersion: Version, requiredVersion: String): Boolean {
        return requiredVersion.isEmpty() || existingVersion.satisfies(requiredVersion)
    }

    private fun addPlugin(pluginMeta: PluginMeta) {
        val pluginId = pluginMeta.pluginId
        val dependencies = pluginMeta.dependencies
        if (dependencies.isEmpty()) {
            dependenciesGraph.addVertex(pluginId)
            dependentsGraph.addVertex(pluginId)
        } else {
            dependencies.forEach { dependency ->
                dependenciesGraph.addEdge(pluginId, dependency.pluginId)
                dependentsGraph.addEdge(dependency.pluginId, pluginId)
            }
        }
    }
}

