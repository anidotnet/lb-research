package org.dizitart.letterbox.core.config

import org.dizitart.letterbox.api.config.ConfigView
import tornadofx.*

/**
 * @author Anindya Chatterjee.
 */
data class ConfigGroup (
        val name: String,
        var view: ConfigView? = null,
        val children: MutableSet<ConfigGroup> = mutableSetOf()
)

data class ConfigGroupChanged(val configGroup: ConfigGroup): FXEvent()