package org.dizitart.letterbox.core.boot

import javafx.scene.image.Image
import javafx.stage.Stage
import javafx.stage.StageStyle
import org.dizitart.letterbox.common.APP_CONFIG_FILE
import org.dizitart.letterbox.common.APP_DATA_DIR
import org.dizitart.letterbox.common.APP_ICON
import org.dizitart.letterbox.core.views.LetterBoxView
import tornadofx.*
import java.nio.file.Path
import java.nio.file.Paths


/**
 *
 * @author Anindya Chatterjee
 */
class LetterBox : App(Image(APP_ICON), LetterBoxView::class) {
    private val starter = LetterBoxStarter()
    private val stageManager = TrayManager()

    override val configBasePath: Path get() = Paths.get(APP_DATA_DIR)
    override val configPath: Path get() = configBasePath.resolve(APP_CONFIG_FILE)!!

    companion object {
        init {
            BootConfigurer.configure()
        }

        @JvmStatic
        fun main(args: Array<String>) {
            // launch the application
            launch(LetterBox::class.java)
        }
    }

    override fun start(stage: Stage) {
        starter.showSplash {
            starter.initializeProviders()
            stage.initStyle(StageStyle.TRANSPARENT)
            super.start(stage)
            stageManager.createSystemTray(resources)
        }
    }
}
