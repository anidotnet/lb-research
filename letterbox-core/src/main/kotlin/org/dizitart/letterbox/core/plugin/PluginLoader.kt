package org.dizitart.letterbox.core.plugin

import mu.KotlinLogging
import java.net.URI
import java.nio.file.FileSystems
import java.nio.file.Files
import java.nio.file.Path

/**
 * @author Anindya Chatterjee.
 */
internal class PluginLoader {
    private val logger = KotlinLogging.logger {}
    private val pluginIndexFileName = "plugins.idx"

    fun loadPluginMetadata(pluginPath: Path): PluginMeta {
        val indexFilePath = getPath(pluginPath, pluginIndexFileName)
                ?: throw PluginException("Can not find plugin index")

        logger.debug { "Looking plugin descriptor in plugin index file $indexFilePath" }
        if (Files.notExists(indexFilePath)) {
            throw PluginException("Cannot find $indexFilePath path")
        }

        val pluginIndexReader = PluginIndexReader(indexFilePath)
        pluginIndexReader.read()
        val manifestType = pluginIndexReader.manifestType
        val pluginId = pluginIndexReader.pluginId
        val pluginVersion = pluginIndexReader.pluginVersion
        val dependencies = pluginIndexReader.dependencies

        return PluginMeta(
                pluginId = pluginId,
                pluginVersion = pluginVersion,
                path = pluginPath,
                manifestType = manifestType,
                dependencies = dependencies
        )
    }

    private fun getPath(path: Path, first: String, vararg more: String): Path? {
        var uri = path.toUri()
        if (isJarFile(path)) {
            var pathString = path.toString()
            // transformation for Windows OS
            pathString = addStart(pathString.replace("\\", "/"), "/") ?: pathString
            // space is replaced with %20
            pathString = pathString.replace(" ".toRegex(), "%20")
            uri = URI.create("jar:file:$pathString")
        }

        return getPath(uri, first, *more)
    }

    private fun getPath(uri: URI, first: String, vararg more: String): Path? {
        val fileSystem = FileSystems.newFileSystem(uri, emptyMap<String, String>())
        return fileSystem.getPath(first, *more)
    }

    /**
     * addStart(null, *)      = *
     * addStart("", *)        = *
     * addStart(*, null)      = *
     * addStart("domain.com", "www.")  = "www.domain.com"
     * addStart("abc123", "abc")    = "abc123"
     */
    private fun addStart(str: String?, add: String?): String? {
        if (add.isNullOrEmpty()) {
            return str
        }

        if (str.isNullOrEmpty()) {
            return add
        }

        return if (str?.startsWith(add!!) == false) {
            add + str
        } else str
    }

    private fun isJarFile(path: Path): Boolean {
        return Files.isRegularFile(path) && path.toString().toLowerCase().endsWith(".jar")
    }
}