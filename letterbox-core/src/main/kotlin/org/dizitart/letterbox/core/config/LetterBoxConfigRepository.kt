package org.dizitart.letterbox.core.config

import mu.KotlinLogging
import org.dizitart.letterbox.api.config.ConfigElement
import org.dizitart.letterbox.api.config.ConfigReaderWriter
import org.dizitart.letterbox.api.config.ConfigRepository
import org.dizitart.letterbox.core.database.LetterBoxStore

/**
 * @author Anindya Chatterjee.
 */
internal class LetterBoxConfigRepository(
        private val readerWriter: ConfigReaderWriter) : ConfigRepository {

    private val logger = KotlinLogging.logger {  }
    private val configElements = mutableMapOf<String, ConfigElement<*>>()
    private var dirty = false

    override fun containsElement(id: String): Boolean = configElements.containsKey(id)

    override fun get(id: String): ConfigElement<*>? {
        return configElements[id]
    }

    override fun set(id: String, element: ConfigElement<*>) {
        configElements[id] = element
    }

    override fun markDirty() {
        dirty = true
    }

    override fun load() {
        logger.info { "Loading configurations from database" }
        try {
            val map = readerWriter.read()
            configElements.putAll(map)
        } catch (e: Exception) {
            logger.error(e) { "Error while loading configuration" }
        }
    }

    override fun save() {
        if (!dirty) return

        logger.info { "Saving configurations to database" }
        try {
            readerWriter.write(configElements)
            dirty = false
        } catch (e: Exception) {
            logger.error(e) { "Error while saving configuration" }
        }
        LetterBoxStore.save()
    }
}