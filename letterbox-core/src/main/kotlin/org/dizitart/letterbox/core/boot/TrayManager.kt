package org.dizitart.letterbox.core.boot

import dorkbox.systemTray.MenuItem
import dorkbox.systemTray.SystemTray
import javafx.application.Platform
import mu.KotlinLogging
import tornadofx.*
import java.awt.event.ActionListener
import java.io.IOException



/**
 * @author Anindya Chatterjee.
 */
class TrayManager {
    private val logger = KotlinLogging.logger {  }

    fun createSystemTray(resources: ResourceLookup) {
        //NOTE: need to install for Ubuntu https://extensions.gnome.org/extension/1031/topicons/

        val systemTray = SystemTray.get() ?:
        throw RuntimeException("Unable to start SystemTray!")

        try {
            val osName = System.getProperty("os.name")
            val iconFileName = when {
                osName.contains("win") -> "/icons/icon.ico"
                else -> "/icons/icon.png"
            }
            systemTray.setImage(resources.stream(iconFileName))
        } catch (e: IOException) {
            logger.error(e) { "Error while loading icon" }
        }

        systemTray.menu.add(MenuItem("Exit", ActionListener {
            systemTray.shutdown()
            Platform.exit()
        }))
    }
}