package org.dizitart.letterbox.core.plugin

import com.github.zafarkhaja.semver.Version
import org.dizitart.letterbox.api.plugin.PluginManifestEntry
import java.nio.file.Path

/**
 * @author Anindya Chatterjee.
 */

data class PluginInfo(val pluginId: String,
                      val pluginMeta: PluginMeta,
                      val pluginManifest: PluginManifestEntry,
                      val classLoader: ClassLoader)

data class PluginMeta(
        val pluginId: String,
        val pluginVersion: Version,
        val path: Path,
        val manifestType: String,
        val dependencies: Set<PluginDependency>
)

data class PluginDependency(
        val pluginId: String,
        val versionSupport: String = "*"
)

data class PluginState(
        val pluginId: String,
        val enabled: Boolean
)

data class WrongDependencyVersion(val dependencyId: String,
                                  val dependentId: String,
                                  val existingVersion: Version,
                                  val requiredVersion: String)