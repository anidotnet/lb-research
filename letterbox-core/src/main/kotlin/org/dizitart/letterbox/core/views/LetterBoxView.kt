package org.dizitart.letterbox.core.views

import javafx.geometry.Insets
import javafx.geometry.NodeOrientation
import javafx.geometry.Orientation
import javafx.geometry.Pos
import javafx.scene.layout.Priority
import org.dizitart.letterbox.api.fx.WindowView
import org.dizitart.letterbox.api.fx.addImage
import org.dizitart.letterbox.api.styles.LetterBoxGraphicsCatalog
import org.dizitart.letterbox.api.styles.LetterBoxStylesheet
import org.dizitart.letterbox.common.APP_NAME
import org.dizitart.letterbox.core.controllers.LetterBoxController
import org.dizitart.letterbox.core.controllers.LetterBoxMenuController
import org.dizitart.letterbox.core.controllers.NotificationController
import tornadofx.*

/**
 * @author Anindya Chatterjee.
 */
class LetterBoxView : WindowView(APP_NAME, titleBarHeight = 40.0) {
    private val controller: LetterBoxController by inject()
    private val menuController: LetterBoxMenuController by inject()
    private val notificationController: NotificationController by inject()

    override val windowName = "mainWindow"

    override val container = anchorpane {
        controller.setContainer(this)
    }

    override val titleBarExtensions = hbox {
        alignment = Pos.CENTER_LEFT
        separator(Orientation.VERTICAL) {
            padding = Insets(5.0)
        }

        menubutton {
            addClass(LetterBoxStylesheet.lbMenuButton,
                    LetterBoxStylesheet.lbImageButton,
                    LetterBoxStylesheet.lbImageButtonNormal)
            addImage(LetterBoxGraphicsCatalog.menuButton)
            nodeOrientation = NodeOrientation.LEFT_TO_RIGHT
            menuController.setPrimaryMenu(this)
        }

        button {
            addClass(LetterBoxStylesheet.lbImageButton, LetterBoxStylesheet.lbImageButtonNormal)
            addImage(LetterBoxGraphicsCatalog.notificationButton)
            notificationController.setNotificationButton(this)
        }

        separator(Orientation.VERTICAL) {
            padding = Insets(5.0)
        }

        controller.serviceButtons.forEach { button ->
            this += button
        }

        region {
            minWidth = 10.0
        }

        hbox {
            hgrow = Priority.ALWAYS
            alignment = Pos.CENTER_LEFT
            nodeOrientation = NodeOrientation.LEFT_TO_RIGHT
            controller.setActionBar(this)
        }
    }

    override fun onDock() {
        super.onDock()
        if (controller.serviceButtons.size > 1) {
            val button = controller.serviceButtons.last()
            button.fire()
        }
    }
}
