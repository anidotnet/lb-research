package org.dizitart.letterbox.core.boot

import de.codecentric.centerdevice.javafxsvg.SvgImageLoaderFactory
import org.dizitart.letterbox.common.APP_DATA_DIR
import org.dizitart.letterbox.core.uri.LetterBoxURLHandlerFactory
import tornadofx.loadFont
import java.io.File
import java.lang.invoke.MethodHandles
import java.net.URL
import java.nio.file.Files
import java.nio.file.Paths

/**
 *
 * @author Anindya Chatterjee
 */
object BootConfigurer {
    fun configure() {
        // set default font rendering
        System.setProperty("prism.lcdtext", "false")
        System.setProperty("prism.subpixeltext", "native")
        System.setProperty("prism.text", "t2k")

        // start svg library
        SvgImageLoaderFactory.install()

        // load fonts
        loadFonts()

        // register url handler
        URL.setURLStreamHandlerFactory(LetterBoxURLHandlerFactory())

        // create data directory if not exists
        if (!Files.exists(Paths.get(APP_DATA_DIR))) {
            Files.createDirectory(Paths.get(APP_DATA_DIR))
        }

        // set up logback by loading configBean xml file
        val configFile: String = APP_DATA_DIR + File.separator + "logback.xml"
        if (!Files.exists(Paths.get(configFile))) {
            // if configBean does not exists in configBean dir
            // extract it from application and place it
            // in the configBean dir to start.

            // It is placed in configBean dir, so that user can
            // change the configBean if required
            val configStream = LetterBox::class.java.
                    classLoader.getResourceAsStream("config/logback.xml")
            Files.copy(configStream, Paths.get(configFile))
        }
        System.setProperty("logback.configurationFile", configFile)
    }

    private fun loadFonts() {
        val fontResources = MethodHandles.lookup().lookupClass().getResource("/fonts/")
        File(fontResources.path).list().forEach {
            loadFont("/fonts/$it", -1.0)
        }
    }
}