package org.dizitart.letterbox.core.views

import javafx.geometry.Insets
import javafx.geometry.NodeOrientation
import javafx.geometry.Orientation
import javafx.scene.control.SplitPane
import org.dizitart.letterbox.api.config.readConfigElement
import org.dizitart.letterbox.api.config.writeConfigElement
import org.dizitart.letterbox.api.fx.ModalView
import org.dizitart.letterbox.api.styles.LetterBoxStylesheet
import org.dizitart.letterbox.core.config.ConfigGroup
import org.dizitart.letterbox.core.config.ConfigGroupChanged
import org.dizitart.letterbox.core.controllers.ConfigController
import tornadofx.*

/**
 * @author Anindya Chatterjee.
 */
class ConfigManagerView : ModalView("Preference") {
    private val controller: ConfigController by inject()
    private lateinit var splitPane: SplitPane

    override val windowName = "preference"

    override val container = vbox {
        splitpane(Orientation.HORIZONTAL) {
            splitPane = this

            treeview<ConfigGroup> {
                root = controller.rootNode
                isShowRoot = false
                cellFormat { text = it.name }

                onUserSelect {
                    fire(ConfigGroupChanged(it))
                }
            }

            vbox {
                controller.container = this
            }

            dividers[0].position = readConfigElement("$windowName.splitter", 0.2)!!
        }
        hbox {
            nodeOrientation = NodeOrientation.RIGHT_TO_LEFT
            padding = Insets(10.0, 0.0, 10.0, 0.0)

            buttonbar {
                buttonMinWidth = 80.0
                button ("Apply") {
                    addClass(LetterBoxStylesheet.lbButton, LetterBoxStylesheet.lbButtonNormal)
                }

                button ("Cancel") {
                    addClass(LetterBoxStylesheet.lbButton, LetterBoxStylesheet.lbButtonNormal)
                }

                button ("Reset") {
                    addClass(LetterBoxStylesheet.lbButton, LetterBoxStylesheet.lbButtonNormal)
                }
            }
        }
    }

    override fun onUndock() {
        super.onUndock()
        writeConfigElement("$windowName.splitter", splitPane.dividers[0].position)
    }
}