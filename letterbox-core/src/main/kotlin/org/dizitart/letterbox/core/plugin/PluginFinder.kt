package org.dizitart.letterbox.core.plugin

import java.io.File
import java.io.FileFilter
import java.nio.file.Path

/**
 * @author Anindya Chatterjee.
 */
internal class PluginFinder {
    private val filters = mutableListOf<FileFilter>()

    init {
        filters.add(JarFileFilter())
    }

    fun findPlugins(path: Path): List<Path> {
        val pluginList = mutableListOf<Path>()

        filters.forEach { filter ->
            val files = path.toFile().listFiles(filter)
            if (files != null && files.isNotEmpty()) {
                files.forEach { file ->
                    pluginList.add(file.toPath())
                }
            }
        }
        return pluginList
    }
}

internal class JarFileFilter : FileFilter {
    override fun accept(pathname: File?): Boolean {
        return pathname?.name?.toLowerCase()?.endsWith(".jar") ?: false
    }
}