package org.dizitart.letterbox.core.plugin

import java.nio.file.Path

/**
 * @author Anindya Chatterjee.
 */
interface PluginManager {
    // add plugin root paths
    fun addPath(path: Path)

    // load plugins from file
    fun loadPlugins()

    // start plugin
    fun startPlugins()

    fun getPluginClassLoader(pluginId: String): PluginClassLoader?

    // stop plugin
    fun stopPlugins()

    val pluginStateManager : PluginStateManager
}
