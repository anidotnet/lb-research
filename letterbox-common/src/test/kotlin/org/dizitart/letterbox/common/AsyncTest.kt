package org.dizitart.letterbox.common

import javafx.stage.Stage
import kotlinx.coroutines.experimental.delay
import kotlinx.coroutines.experimental.runBlocking
import org.dizitart.letterbox.common.concurrent.schedule
import org.junit.Test
import org.testfx.api.FxToolkit
import java.time.Duration

/**
 *
 * @author Anindya Chatterjee
 */
class AsyncTest {
    val primaryStage: Stage = FxToolkit.registerPrimaryStage()

    @Test
    fun testSchedule() {
        runBlocking {
            schedule(Duration.ofSeconds(1L), until = { i -> i < 10L }) {
                println("Now " + System.currentTimeMillis())
            }

            schedule(Duration.ofSeconds(2L), until = { i -> i < 10L }) {
                println("Now ${Math.random()}")
            }
            delay(10000)
        }
    }
}