package org.dizitart.letterbox.common.collections

import javafx.collections.ObservableList

/**
 *
 * @author Anindya Chatterjee
 */
inline infix fun <reified T> ObservableList<out T>.contentEquals(other: ObservableList<out T>?): Boolean {
    if (other == null) return false
    return this.toTypedArray() contentEquals other.toTypedArray()
}

inline infix fun <reified T> List<T>.contentEquals(other: ObservableList<out T>?): Boolean {
    if (other == null) return false
    return this.toTypedArray() contentEquals other.toTypedArray()
}

inline infix fun <reified T> ObservableList<out T>.contentEquals(other: List<T>?): Boolean {
    if (other == null) return false
    return this.toTypedArray() contentEquals other.toTypedArray()
}