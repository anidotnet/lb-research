package org.dizitart.letterbox.common.i18n

import javafx.beans.binding.Bindings
import javafx.beans.binding.StringBinding
import javafx.beans.property.ObjectProperty
import javafx.beans.property.SimpleObjectProperty
import java.text.MessageFormat
import java.util.*
import java.util.concurrent.Callable


/**
 * Example: https://stackoverflow.com/documentation/javafx/5434/internationalization-in-javafx#t=201706270154467752674
 * @author Anindya Chatterjee
 */
object I18N {

    private val locale: ObjectProperty<Locale>

    init {
        locale = SimpleObjectProperty(defaultLocale)
        locale.addListener { _, _, newValue -> Locale.setDefault(newValue) }
    }

    val supportedLocales: List<Locale>
        get() {
            return ArrayList<Locale>(Languages.values().map { it.locale })
        }

    val defaultLocale: Locale
        get() {
            val sysDefault = Locale.getDefault()
            return if (supportedLocales.contains(sysDefault)) sysDefault else Locale.ENGLISH
        }

    fun getLocale(): Locale {
        return locale.get()
    }

    fun setLocale(locale: Locale) {
        localeProperty().set(locale)
        Locale.setDefault(locale)
    }

    fun localeProperty(): ObjectProperty<Locale> {
        return locale
    }

    fun get(key: String, vararg args: Any): String {
        val bundle = ResourceBundle.getBundle("messages", getLocale())
        return MessageFormat.format(bundle.getString(key), *args)
    }

    fun createStringBinding(key: String, vararg args: Any): StringBinding {
        return Bindings.createStringBinding(Callable { get(key, args) }, locale)
    }

    fun createStringBinding(func: Callable<String>): StringBinding {
        return Bindings.createStringBinding(func, locale)
    }
}