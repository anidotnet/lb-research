package org.dizitart.letterbox.common.di

import kotlin.reflect.KClass

/**
 *
 * @author Anindya Chatterjee
 */
data class BeanDefinition<out T>(
        val name: String = "",
        val clazz: KClass<*>,
        val beanType: BeanType = BeanType.Singleton,
        val builder: BeanBuilder<T>
) {

    override fun toString(): String {
        val n = if (name.isBlank()) "" else "name='$name', "
        return "$beanType[${n}class=${clazz.java.canonicalName}]"
    }

    override fun equals(other: Any?): Boolean {
        return if (other is BeanDefinition<*>) {
            name == other.name
                    && clazz == other.clazz
                    && beanType == other.beanType
        } else false
    }

    override fun hashCode(): Int {
        return name.hashCode() + clazz.hashCode() + beanType.hashCode()
    }
}

typealias BeanBuilder<T> = () -> T