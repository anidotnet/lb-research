package org.dizitart.letterbox.common.di

/**
 * @author Anindya Chatterjee.
 */
internal class BeanFactory {
    private val singletonMap = mutableMapOf<String, Any>()
    private val beans = mutableListOf<Bean>()

    fun create(definition: BeanDefinition<*>): Any {
        return when (definition.beanType) {
            BeanType.Singleton -> singleton(definition)
            BeanType.Prototype -> {
                try {
                    val instance = definition.builder.invoke()
                    if (instance is Bean) {
                        instance.init()
                        beans.add(instance)
                    }
                    instance
                } catch (t: Throwable) {
                    throw BeanCreationException("Can't instantiate bean $definition", t)
                }
            }
        } ?: throw NoBeanDefFoundException("No bean definition found to resolve type ${definition.clazz.java.canonicalName}")
    }

    fun destroy() {
        beans.forEach(Bean::close)
        singletonMap.clear()
        beans.clear()
    }

    private fun singleton(definition: BeanDefinition<*>): Any {
        val clazzName = definition.clazz.java.canonicalName
        var instance = singletonMap[clazzName]
        if (instance == null) {
            try {
                instance = definition.builder.invoke()
                if (instance is Bean) {
                    instance.init()
                    beans.add(instance)
                }
            } catch (t: Throwable) {
                throw BeanCreationException("Can't instantiate bean $definition", t)
            }
            singletonMap[clazzName] = instance!!
        }
        return instance
    }
}