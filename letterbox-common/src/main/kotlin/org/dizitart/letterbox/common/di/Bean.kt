package org.dizitart.letterbox.common.di

/**
 *
 * @author Anindya Chatterjee
 */
interface Bean {
    fun init()
    fun close()
}

interface EagerBean : Bean