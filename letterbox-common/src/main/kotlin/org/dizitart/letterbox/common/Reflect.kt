package org.dizitart.letterbox.common

import kotlin.reflect.KClass

/**
 *
 * @author Anindya Chatterjee
 */
fun <T : Annotation> KClass<*>.hasAnnotation(clazz: KClass<T>) : Boolean {
    return this.java.getDeclaredAnnotationsByType(clazz.java).isNotEmpty()
}

fun <T : Annotation> Class<*>.hasAnnotation(clazz: KClass<T>) : Boolean {
    return this.getDeclaredAnnotationsByType(clazz.java).isNotEmpty()
}