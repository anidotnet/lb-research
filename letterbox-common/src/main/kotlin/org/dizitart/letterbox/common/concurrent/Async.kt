package org.dizitart.letterbox.common.concurrent

import kotlinx.coroutines.experimental.CommonPool
import kotlinx.coroutines.experimental.Job
import kotlinx.coroutines.experimental.delay
import kotlinx.coroutines.experimental.launch
import java.time.Duration
import java.util.concurrent.TimeUnit
import kotlinx.coroutines.experimental.javafx.JavaFx as UI

/**
 *
 * https://github.com/Kotlin/kotlinx.coroutines/blob/master/core/coroutines-guide-core.md
 * https://discuss.kotlinlang.org/t/coroutines-with-time-scheduling/2942/7
 * @author Anindya Chatterjee.
 */

typealias Until = (Long) -> Boolean

fun schedule(frequency: Duration,
             until: Until = { true },
             block: suspend () -> Unit): Job {
    return launch(CommonPool) {
        var iteration = 1L
        while (until(iteration)) {
            block()
            delay(frequency.toMillis(), TimeUnit.MILLISECONDS)
            iteration++
        }
    }
}

fun scheduleFx(frequency: Duration,
             until: Until = { true },
             block: suspend () -> Unit): Job {
    return launch(UI) {
        var iteration = 1L
        while (until(iteration)) {
            block()
            delay(frequency.toMillis(), TimeUnit.MILLISECONDS)
            iteration++
        }
    }
}