package org.dizitart.letterbox.common

import mu.KotlinLogging
import java.util.*

/**
 * @author Anindya Chatterjee.
 */
internal object Terminator {
    private val logger = KotlinLogging.logger {  }
    private var taskMap = TreeMap<Int, MutableList<ShutdownTask>>()

    init {
        Runtime.getRuntime().addShutdownHook(Thread {
            // create the shutdown hook
            runBeforeShutdown()
        })
    }

    fun registerTask(task: ShutdownTask) {
        // add the task to the map
        var taskList = mutableListOf<ShutdownTask>()
        if (taskMap.containsKey(task.priority)) {
            taskList = taskMap[task.priority] ?: arrayListOf()
        }
        taskList.add(task)
        taskMap[task.priority] = taskList
    }

    private fun runBeforeShutdown() {
        // run the task from list before shutdown
        taskMap.keys.forEach { priority ->
            val taskList = taskMap[priority]
            taskList?.forEach { task ->
                try {
                    task.execute()
                } catch (t: Throwable) {
                    logger.error(t) { "Error while shutdown task - $task"}
                }
            }
        }
    }
}

internal data class ShutdownTask(
        val priority: Int,
        val execute: () -> Unit
)

fun beforeTerminate(priority: Int = Int.MIN_VALUE, execute: () -> Unit) {
    val shutdownTask = ShutdownTask(priority, execute)
    Terminator.registerTask(shutdownTask)
}