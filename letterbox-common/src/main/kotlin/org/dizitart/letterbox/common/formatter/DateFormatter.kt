package org.dizitart.letterbox.common.formatter

import java.time.LocalDate
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter
import java.time.temporal.ChronoUnit

/**
 *
 * @author Anindya Chatterjee
 */
fun ZonedDateTime.toSummeryDate(): String {
    val days = ChronoUnit.DAYS.between(this.toLocalDate(), LocalDate.now())
    val formatter = if (days >= 1) {
        DateTimeFormatter.ofPattern("dd MMM yyyy")
    } else {
        DateTimeFormatter.ofPattern("hh:mma")
    }
    return this.format(formatter)
}

fun ZonedDateTime.toLongDateTime(): String {
    return this.format(DateTimeFormatter.ofPattern("dd MMM yyyy hh:mma"))
}