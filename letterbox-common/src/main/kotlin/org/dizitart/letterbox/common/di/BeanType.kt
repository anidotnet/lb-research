package org.dizitart.letterbox.common.di

/**
 *
 * @author Anindya Chatterjee
 */
enum class BeanType {
    Singleton,
    Prototype
}