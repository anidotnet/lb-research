package org.dizitart.letterbox.themes.darkside

import org.dizitart.letterbox.api.plugin.*
import org.dizitart.letterbox.common.di.beans
import org.dizitart.letterbox.darkside.appVersion

/**
 * @author Anindya Chatterjee.
 */
@PluginManifest(uniqueId = "dark-side-theme", version = appVersion)
class DarkSideManifest : PluginManifestEntry {

    override fun start() {
        // nothing
    }

    override fun stop() {
        // nothing
    }

    override val descriptor: PluginDescriptor
        get() {
            val authorInfo = AuthorInfo("Dizitart", "Dizitart", "https://www.dizitart.org")
            val description = Description(
                    text = "Dark Side theme is a default theme for LetterBox",
                    releaseNote =
                    """
                         1.  Initial release
                    """.trimIndent(),
                    license = "Apache License v2")

            return PluginDescriptor("LetterBox Dark Side theme", authorInfo, description)
        }

    override val beanCatalog = beans {
        singleton { DarkSideLetterBoxGraphics() }
        singleton { DarkSideServiceGraphics() }
        singleton { DarkSideConfig() }
        singleton { DarkSideTheme(get()) }
        singleton { NunitoFont() }
    }
}