package org.dizitart.letterbox.themes.darkside

import javafx.scene.paint.Color
import org.dizitart.letterbox.api.styles.LetterBoxPalette
import tornadofx.*

/**
 *
 * @author Anindya Chatterjee
 */
open class DarkSidePalette : Stylesheet(), LetterBoxPalette {
    override val transparent: Color = Color.TRANSPARENT

    override val controlBackground: Color = transparent
    override val controlBorder: Color = transparent

    override val imageButtonBorder: Color = transparent
    override val imageButtonBorderHover: Color = transparent
    override val imageButtonBorderFocused: Color = transparent

    override val imageButtonBackground: Color = transparent
    override val imageButtonBackgroundHover: Color = transparent
    override val imageButtonBackgroundFocused: Color = transparent

    override val buttonBorder: Color = Color.DARKGRAY
    override val buttonBorderHover: Color = Color.valueOf("#03a9f4")
    override val buttonBorderFocused: Color = Color.valueOf("#03a9f4")

    override val buttonBackground: Color = transparent
    override val buttonBackgroundHover: Color = Color.valueOf("#03a9f4")
    override val buttonBackgroundFocused: Color = Color.valueOf("#03a9f4")

    override val buttonText: Color = Color.BLACK
    override val buttonTextHover: Color = Color.WHITE
    override val buttonTextFocused: Color = Color.WHITE
}