package org.dizitart.letterbox.themes.darkside

import org.dizitart.letterbox.api.config.Default
import org.dizitart.letterbox.api.styles.LetterBoxFont

/**
 *
 * @author Anindya Chatterjee
 */
@Default
class NunitoFont : LetterBoxFont() {
    override val name: String = "Nunito"
    override val fontPaths: List<String> = listOf(
            "fonts/Nunito-Bold.ttf",
            "fonts/Nunito-Regular.ttf"
    )
}