package org.dizitart.letterbox.themes.darkside

import org.dizitart.letterbox.api.config.Default
import org.dizitart.letterbox.api.styles.LetterBoxStylesheet
import tornadofx.px

/**
 *
 * @author Anindya Chatterjee
 */
@Default
open class DarkSideTheme(private val themeConfig: DarkSideConfig) : LetterBoxStylesheet(DarkSidePalette()) {
    init {
        root {
            fontFamily = themeConfig.fontFamily!!
            fontSize = (themeConfig.fontSize!!).px
        }
    }
}