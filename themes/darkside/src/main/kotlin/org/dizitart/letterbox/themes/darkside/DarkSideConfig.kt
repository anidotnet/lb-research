package org.dizitart.letterbox.themes.darkside

import org.dizitart.letterbox.api.config.Configuration
import org.dizitart.letterbox.common.DEFAULT_FONT_FAMILY
import org.dizitart.letterbox.common.DEFAULT_FONT_SIZE

/**
 *
 * @author Anindya Chatterjee
 */
class DarkSideConfig : Configuration {
    val fontFamily: String? by configElement(DEFAULT_FONT_FAMILY, "Noto Sans")
    val fontSize: Double? by configElement(DEFAULT_FONT_SIZE, -1.0)
}