package org.dizitart.letterbox.themes.darkside

import javafx.scene.image.Image
import org.dizitart.letterbox.api.config.Default
import org.dizitart.letterbox.api.styles.IconSet

/**
 *
 * @author Anindya Chatterjee
 */
@Default
class MaterialIcons : IconSet {

    override val attachmentImage: Image = Image("images/attach.svg", IconSet.SIZE_14, IconSet.SIZE_14, true, true)

    override val warningImage: Image = Image("images/warning.svg", IconSet.SIZE_20, IconSet.SIZE_20, true, true)

    override val replyImage: Image = Image("images/reply.svg", IconSet.SIZE_20, IconSet.SIZE_20, true, true)

    override val replyAllImage: Image = Image("images/reply-all.svg", IconSet.SIZE_20, IconSet.SIZE_20, true, true)

    override val forwardImage: Image = Image("images/forward.svg", IconSet.SIZE_20, IconSet.SIZE_20, true, true)

    override val labelOutlineImage: Image = Image("images/label-outline.svg", IconSet.SIZE_18, IconSet.SIZE_18, true, true)

    override val blockImage: Image = Image("images/block.svg", IconSet.SIZE_18, IconSet.SIZE_18, true, true)

    override val infoImage: Image = Image("images/info.svg", IconSet.SIZE_18, IconSet.SIZE_18, true, true)

    override val trashImage: Image = Image("images/trash.svg", IconSet.SIZE_18, IconSet.SIZE_18, true, true)

    override val ascendingImage: Image = Image("images/ascending.svg", IconSet.SIZE_18, IconSet.SIZE_18, true, true)

    override val dateImage: Image = Image("images/date.svg", IconSet.SIZE_18, IconSet.SIZE_18, true, true)

    override val subjectImage: Image = Image("images/subject.svg", IconSet.SIZE_18, IconSet.SIZE_18, true, true)

    override val personImage: Image = Image("images/from.svg", IconSet.SIZE_18, IconSet.SIZE_18, true, true)

    override val sortImage: Image = Image("images/sort.svg", IconSet.SIZE_18, IconSet.SIZE_18, true, true)

    override val filterImage: Image = Image("images/filter.svg", IconSet.SIZE_18, IconSet.SIZE_18, true, true)

    override val noFilterImage: Image = Image("images/no-filter.svg", IconSet.SIZE_18, IconSet.SIZE_18, true, true)

    override val unreadImage: Image = Image("images/unread.svg", IconSet.SIZE_18, IconSet.SIZE_18, true, true)

    override val favoriteImage: Image = Image("images/favorite.svg", IconSet.SIZE_18, IconSet.SIZE_18, true, true)

    override val favoriteImageSmall: Image = Image("images/favorite.svg", IconSet.SIZE_10, IconSet.SIZE_10, true, true)

    override val favoriteImageLight: Image = Image("images/favorite-menu.svg", IconSet.SIZE_18, IconSet.SIZE_18, true, true)

    override val notFavoriteImage: Image = Image("images/not-favorite.svg", IconSet.SIZE_18, IconSet.SIZE_18, true, true)

    override val backImage: Image = Image("images/arrow-left.svg", IconSet.SIZE_20, IconSet.SIZE_20, true, true)

    override val rightImage: Image = Image("images/arrow-right.svg", IconSet.SIZE_20, IconSet.SIZE_20, true, true)

    override val syncImage: Image = Image("images/sync.svg", IconSet.SIZE_18, IconSet.SIZE_18, true, true)

    override val selectAllImage: Image = Image("images/select-all.svg", IconSet.SIZE_18, IconSet.SIZE_18, true, true)

    override val newItemImage: Image = Image("images/new-item.svg", IconSet.SIZE_25, IconSet.SIZE_25, true, true)

    override val verticalMenuImage: Image = Image("images/menu-vert.svg", IconSet.SIZE_20, IconSet.SIZE_20, true, true)

    override val saveAddressImage: Image = Image("images/save.svg", IconSet.SIZE_18, IconSet.SIZE_18, true, true)

    override val sendEmailImage: Image = Image("images/email-send.svg", IconSet.SIZE_18, IconSet.SIZE_18, true, true)

    override val emailAddressImage: Image = Image("images/email-address.svg", IconSet.SIZE_14, IconSet.SIZE_14, true, true)

    override val inboxFolderImage: Image = Image("images/inbox.svg", IconSet.SIZE_14, IconSet.SIZE_14, true, true)

    override val sentFolderImage: Image = Image("images/sent.svg", IconSet.SIZE_14, IconSet.SIZE_14, true, true)

    override val draftFolderImage: Image = Image("images/drafts.svg", IconSet.SIZE_14, IconSet.SIZE_14, true, true)

    override val outboxFolderImage: Image = Image("images/outbox.svg", IconSet.SIZE_14, IconSet.SIZE_14, true, true)

    override val spamFolderImage: Image = Image("images/spam.svg", IconSet.SIZE_14, IconSet.SIZE_14, true, true)

    override val trashFolderImage: Image = Image("images/trash-folder.svg", IconSet.SIZE_14, IconSet.SIZE_14, true, true)

    override val folderImage: Image = Image("images/folder.svg", IconSet.SIZE_14, IconSet.SIZE_14, true, true)

    override val closeImage: Image = Image("images/close.svg", IconSet.SIZE_14, IconSet.SIZE_14, true, true)

    override val nextImage: Image = Image("images/next.svg", IconSet.SIZE_18, IconSet.SIZE_18, true, true)

    override val previousImage: Image = Image("images/previous.svg", IconSet.SIZE_18, IconSet.SIZE_18, true, true)

    override val finishImage: Image = Image("images/finish.svg", IconSet.SIZE_18, IconSet.SIZE_18, true, true)

    override val menuImage: Image = Image("images/menu.svg", IconSet.SIZE_24, IconSet.SIZE_24, true, true)

    override val emailButtonImage: Image = Image("images/email-button.svg", IconSet.SIZE_30, IconSet.SIZE_30, true, true)

    override val calendarButtonImage: Image = Image("images/calendar.svg", IconSet.SIZE_30, IconSet.SIZE_30, true, true)

    override val taskButtonImage: Image = Image("images/task.svg", IconSet.SIZE_30, IconSet.SIZE_30, true, true)

    override val notesButtonImage: Image = Image("images/notes.svg", IconSet.SIZE_30, IconSet.SIZE_30, true, true)

    override val settingsImage: Image = Image("images/settings.svg", IconSet.SIZE_20, IconSet.SIZE_20, true, true)

    override val pluginImage: Image = Image("images/plugins.svg", IconSet.SIZE_20, IconSet.SIZE_20, true, true)

    override val accountImage: Image = Image("images/account.svg", IconSet.SIZE_20, IconSet.SIZE_20, true, true)

    override val name: String = "MaterialIcons"
}